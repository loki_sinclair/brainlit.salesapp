const Api = {
    get: async function(route) {
        return fetch('http://localhost:8989' + route, {
            method: 'get',
            headers: this.makeHeaders()
        }).then(async (response) => {            
            return this.checkResponseCode(response);
        }).then((responseJson) => {
            return responseJson;
        }).catch((error) => {
            console.log(error);
            return error;
        });
    },

    post: function(route, params) {
        return fetch('http://localhost:8989' + route, {
            method: 'post',
            headers: this.makeHeaders(),
            body: JSON.stringify(params)
        }).then((response) => {
            return this.checkResponseCode(response);
        }).then((responseJson) => {
            return responseJson;
        }).catch((error) => {
            console.log(error);
            return error;
        });
    },

    delete: function(route) {
        return fetch('http://localhost:8989' + route, {
            method: 'delete',
            headers: this.makeHeaders(),
        }).then((response) => {
            return this.checkResponseCode(response);
        }).then((responseJson) => {
            console.log(responseJson);
            return responseJson;
        }).catch((error) => {
            console.log(error);
            return error;
        });
    },

    checkResponseCode(response) {
        switch(response.status) {
            case 401:
                // Unauthorized, therefore, we blatt the token
                // and redirect to the login page
                localStorage.removeItem('token');
                window.location.href = '/';
            default:
                return response.json();
        }
    },

    getToken: function() {
        var token = localStorage.getItem("token")
        if (token) {
            return token;
        }

        return null;
    },

    makeHeaders: function() {
        return new Headers({
            'Authorization': 'Bearer ' + this.getToken()
        });
    }
}

export default Api;