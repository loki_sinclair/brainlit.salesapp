import React from 'react';

class AddRoom extends React.Component {
    constructor(props) {
        super(props);

        this.state = {};
    };

    render() {
        return (
            <div className="container">            
                <div className="panel panel-primary">
                    <div className="panel-heading">
                        <h3 className="panel-title">Add Room</h3>
                    </div>
                    <div className="panel-body">
                        <div className="alert alert-default">
                            Add rooms to existing projects
                        </div>
                        <div className="form-group">
                            <div className="container">
                                <div className="row">
                                    <select name="projects" className="form-control">                                    
                                        { 
                                            this.props.projects == null || this.props.projects.length <= 0 &&
                                                <option value="null">Loading...</option>

                                        }
                                        { 
                                            this.props.projects != null && this.props.projects.length > 0 && this.props.projects.map((proj) => {
                                                <option value={proj.Id}>{proj.Name}</option>
                                            })
                                        }
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
};

export default AddRoom;