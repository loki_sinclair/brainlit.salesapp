import React from 'react';

import EmptyRoom from './emptyRoom';

import './blockDetails.css';
import './emptyRoom.css';

class BlockDetails extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            block: this.props.Block,
            addingRoom: false,
        };

        this.handleAddRoomClick = this.handleAddRoomClick.bind(this);
        this.renderRooms = this.renderRooms.bind(this);
    }

    async handleAddRoomClick() {
        var room = {
            id: -100,
            name: '*New Room',
            roomType: -1,
            projectId: this.state.block.ProjectId,
            blockId: this.state.block.ID,
            groupId: -1,
            roomLength: 0,
            roomWidth: 0,
            ceilingHeight: 0,
            wallColor: '#ffffff',
            wallColorRgb: '255,255,255',
            ceilingColor: '#ffffff',
            ceilingColorRgb: '255,255,255',
            floorColor: '#ffffff',
            floorColorRgb: '255,255,255',
            numSensor: 1,
            numKeypads: 1,
            numLuminaires: 0,
        };

        this.state.block.rooms.push(room);
        this.setState({block: this.state.block});
    }

    render() {
        return (
            <div>
                <div className="row">
                    <span className="add-room">
                        <button type="button" className="btn btn-success pull-right"
                            onClick={this.handleAddRoomClick}>Add Room
                        </button>
                    </span>
                </div>
                <div className="row">
                    {this.renderRooms()}
                </div>
            </div>
        );
    }

    renderRooms() {
        if (this.state.block.rooms && this.state.block.rooms.length > 0) {
            return (
                <div>
                    {this.state.block.rooms.map((room) => {
                        return (
                            <EmptyRoom
                                key={room.ID}
                                block={this.state.block}
                                room={room} 
                                projectId={this.state.block.ProjectID}/>
                        );
                    })}
                </div>
            );
        } else {
            return (
                <div className="text-center">
                    <strong>{this.state.block.Name}</strong> has no assigned rooms yet! You can add rooms using the button above.
                </div>
            );
        }
    }
};

export default BlockDetails;