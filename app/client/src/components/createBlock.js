import React from 'react';

import './createBlock.css';
import Api from '../common/api';

class CreateBlock extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            projectId: this.props.ProjectId,
            blockName: '',
        };

        this.handleCreateBlock = this.handleCreateBlock.bind(this);
        this.handleBlockNameChanged = this.handleBlockNameChanged.bind(this);
    }

    handleBlockNameChanged(event) {
        this.setState({blockName: event.target.value});
    }

    async handleCreateBlock() {
        await Api.post('/projectblocks/create', {
            projectId: this.state.projectId,
            blockName: this.state.blockName,
        });
        this.props.ReloadBlocks(this.state.projectId);
        this.setState({blockName: ''});
    }

    render() {
        return (
            <div className="create-block-form">
                <div className="form-group">
                    <label htmlFor="blockName">Name</label>
                    <input type="text" className="form-control" name="blockName" onChange={this.handleBlockNameChanged}/>
                    <br/>
                    <button type="button" className="btn btn-success pull-right" onClick={this.handleCreateBlock}>Create</button>
                </div>
            </div>
        );
    }
}

export default CreateBlock;