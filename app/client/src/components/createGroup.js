import React from 'react';

import './createGroup.css';
import Api from '../common/api';

class CreateGroup extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            projectId: this.props.ProjectId,
            groupName: '',
        };

        this.handleCreateGroup = this.handleCreateGroup.bind(this);
        this.handleGroupNameChanged = this.handleGroupNameChanged.bind(this);
    }

    async handleCreateGroup() {
        await Api.post('/projectgroups/create', {
            projectId: this.state.projectId,
            groupName: this.state.groupName,
        });
        this.setState({groupName: ''});
    }

    handleGroupNameChanged(event) {
        this.setState({groupName: event.target.value});
    }

    render() {
        return (
            <div className="create-group-form">
                <div className="form-group">
                    <label htmlFor="groupName">Name</label>
                    <input type="text" className="form-control" name="groupName" onChange={this.handleGroupNameChanged} />
                    <br/>
                    <button type="button" className="btn btn-success pull-right" onClick={this.handleCreateGroup}>Create</button>
                </div>
            </div>
        );
    }
}

export default CreateGroup;