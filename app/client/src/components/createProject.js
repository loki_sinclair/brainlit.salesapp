import React from 'react';
import Api from '../common/api';

class CreateProject extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            name: '',
            location: '',
            timezone: '',
            coords: ''
        };

        this.handleProjectSave = this.handleProjectSave.bind(this);
        this.handleNameChange = this.handleNameChange.bind(this);
        this.handleLocationChange = this.handleLocationChange.bind(this);
        this.handleTimezoneChange = this.handleTimezoneChange.bind(this);
        this.handleCoordsChange = this.handleCoordsChange.bind(this);
    }

    handleNameChange(event) {
        this.setState({name: event.target.value});
    }

    handleLocationChange(event) {
        this.setState({location: event.target.value});
    }

    handleTimezoneChange(event) {
        this.setState({timezone: event.target.value});
    }

    handleCoordsChange(event) {
        this.setState({coords: event.target.value});
    }

    async handleProjectSave(event) {
        await Api.post('/projects/create', {
            name: this.state.name,
            location: this.state.location,
            timezone: this.state.timezone,
            coords: this.state.coords,
        });
    }

    render() {
        return (
            <div className="container">
                <h3>New Project</h3>
                <div className="form-group">
                    <label htmlFor="projectName">Name</label>
                    <input type="text"
                        className="form-control"
                        id="projectName"
                        placeholder="Unnamed Project"
                        onChange={this.handleNameChange}/>
                </div>
                <div className="form-group">
                    <label htmlFor="projectLocation">Location</label>
                    <input type="text"
                        className="form-control"
                        id="projectLocation"
                        placeholder="Location"
                        onChange={this.handleLocationChange}/>
                </div>
                <div className="form-group">
                    <label htmlFor="projectTimezone">Timezone</label>
                    <input type="text"
                        className="form-control"
                        id="projectTimezon"
                        placeholder="Timezone"
                        onChange={this.handleTimezoneChange}/>
                </div>
                <div className="form-group">
                    <label htmlFor="projectCoords">Coordinates (Lat/Long)</label>
                    <input type="text"
                        className="form-control"
                        id="projectCoords"
                        placeholder="Lat/Long"
                        onChange={this.handleCoordsChange}/>
                </div>
                <button type="submit"
                    className="btn btn-success pull-right"
                    onClick={this.handleProjectSave}
                >Save</button>
            </div>
        );
    }
}

export default CreateProject;