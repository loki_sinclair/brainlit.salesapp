import React from 'react';
import Modal from 'react-modal';
import {CirclePicker} from 'react-color';
import InputRange from 'react-input-range';

import RoomTypeSelect from './roomTypesSelect';
import GroupSelect from './groupSelect';

import Api from '../common/api';

import { library } from '@fortawesome/fontawesome-svg-core'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faPencilAlt, faPen } from '@fortawesome/free-solid-svg-icons'

library.add(faPencilAlt, faPen);

import 'react-input-range/lib/css/index.css';
import '../routes/calculator/calculator.css';
import './emptyRoom.css';

import roomImage from '../images/noun_room_125127.png';

const customModalStyle = {
    content: {
        top: '50%',
        left: '50%',
        right: 'auto',
        bottom: 'auto',
        marginRight: '-50%',
        transform: 'translate(-50%, -50%)'
    }
};

const defaultColors = [
    '#FFFFFF',
    '#F5F5F5',
    '#DCDCDC',
    '#D3D3D3',
    '#C0C0C0',
    '#A9A9A9',
    '#808080',
    '#505050',
    '#303030',
    '#000000',
];

class EmptyRoom extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            RoomId: this.props.room.id,
            RoomName: this.props.room.name,
            RoomType: this.props.room.roomType,
            RoomLength: this.props.room.roomLength,
            RoomWidth: this.props.room.roomWidth,
            CeilingHeight: this.props.room.ceilingHeight,
            WallColorHex: this.props.room.wallColorHex,
            WallColorRgb: this.props.room.wallColorRgb,
            FloorColorHex: this.props.room.floorColorHex,
            FloorColorRgb: this.props.room.floorColorRgb,
            CeilingColorHex: this.props.room.ceilingColorHex,
            CeilingColorRgb: this.props.room.ceilingColorRgb,
            NumSensor: this.props.room.numSensor,
            NumKeypads: this.props.room.numKeypads,
            NumLuminaires: this.props.room.numLuminaires,
            GroupId: this.props.room.groupId,
            ProjectId: this.props.projectId,
            BlockId: this.props.block.ID,
            modalActive: false,
            calculateThrottle: false,
        };

        this.handleRoomNameChange = this.handleRoomNameChange.bind(this);

        this.handleCeilingColorChange = this.handleCeilingColorChange.bind(this);
        this.handleWallColorChange = this.handleWallColorChange.bind(this);
        this.handleFloorColorChange = this.handleFloorColorChange.bind(this);
        this.handleRoomTypeChange = this.handleRoomTypeChange.bind(this);
        this.handleGroupChange = this.handleGroupChange.bind(this);

        this.handleCancelClick = this.handleCancelClick.bind(this);
        this.handleSaveClick = this.handleSaveClick.bind(this);

        this.openModal = this.openModal.bind(this);
        this.closeModal = this.closeModal.bind(this);
    }

    handleSaveClick() {
        // Send back up the chain
        var room = {
            id: this.state.RoomId,
            Name: this.state.RoomName,
            RoomLength: this.state.RoomLength,
            RoomWidth: this.state.RoomWidth,
            CeilingHeight: this.state.CeilingHeight,
            WallColorHex: this.state.WallColorHex,
            CeilingColorHex: this.state.CeilingColorHex,
            FloorColorHex: this.state.FloorColorHex,
            WallColorRgb: this.state.WallColorRgb,
            CeilingColorRgb: this.state.CeilingColorRgb,
            FloorColorRgb: this.state.FloorColorRgb,
            RoomType: this.state.RoomType,
            NumSensor: this.state.NumSensor,
            NumKeypads: this.state.NumKeypads,
            NumLuminaires: this.state.results.numLuminaires,
            GroupId: this.state.GroupId,
            ProjectId: this.state.ProjectId,
            BlockId: this.state.BlockId,
        };

        // Write to db
        this.saveRoom(room);
        this.closeModal();
    }

    handleCancelClick() {
        this.closeModal();
    }

    handleGroupChange(event) {
        this.setState({GroupId: parseInt(event.target.value)});
    }

    handleRoomTypeChange(event) {
        this.setState({RoomType: parseInt(event.target.value), calculateThrottle: false});
    }

    handleRoomLengthChange(event) {
        this.setState({RoomLength: event.target.value, calculateThrottle: false});
    }

    handleRoomWidthChange(event) {
        this.setState({RoomWidth: event.target.value, calculateThrottle: false});
    }

    handleRoomNameChange(event) {
        this.setState({RoomName: event.target.value, calculateThrottle: false});
    }

    handleCeilingColorChange(color) {
        this.setState({
            CeilingColorHex: color.hex,
            CeilingColorRgb: color.rgb.r +
            "," + color.rgb.g + "," + color.rgb.b,
            calculateThrottle: false,
        });
    }

    handleWallColorChange(color) {
        this.setState({
            WallColorHex: color.hex,
            WallColorRgb: color.rgb.r +
            "," + color.rgb.g + "," + color.rgb.b,
            calculateThrottle: false,
        });
    }


    handleFloorColorChange(color) {
        this.setState({
            FloorColorHex: color.hex,
            FloorColorRgb: color.rgb.r +
            "," + color.rgb.g + "," + color.rgb.b,
            calculateThrottle: false,
        });
    }

    openModal() {
        this.setState({modalActive: true});
    }

    closeModal() {
        this.setState({modalActive: false});
    }

    async saveRoom(room) {
        const response = await Api.post('/room', room);
        this.setState({RoomId: response.id});
    }

    async calculateLuminaires() {
        const response = await Api.post('/calculate_luminaires', {
            ceilingColor: this.state.CeilingColorRgb,
            wallColor: this.state.WallColorRgb,
            floorColor: this.state.FloorColorRgb,
            roomLength: this.state.RoomLength,
            roomWidth: this.state.RoomWidth,
            ceilingHeight: this.state.CeilingHeight,
            requiredLux: 1078, // TODO: THIS NEEDS TO BE TAKEN FROM DB
        });
        this.setState({results: response.data});
    }

    render() {
        return (
            <div className="empty-room">
                <img src={roomImage} className="empty-room-image" />
                <div className="room-edit-icon">
                    <a onClick={this.openModal}><FontAwesomeIcon icon="pencil-alt"/></a>
                </div>
                <div className="room-name">
                    <span>{this.state.RoomName}</span>
                </div>
                <Modal
                    isOpen={this.state.modalActive}
                    onRequestClose={this.closeModal}
                    contentLabel="Edit Room"
                    style={customModalStyle}
                    >
                        <div className="container">
                            <div className="form-group metric-side">
                                <div className="row">
                                    <h2>Room Details</h2>
                                    <hr/>
                                </div>

                                <div className="row">
                                    <label htmlFor="roomName">Room Name</label>
                                    <input type="text" className="form-control"
                                        name="roomName"
                                        value={this.state.RoomName}
                                        onChange={this.handleRoomNameChange} />
                                </div>
                                <div className="row">
                                    <label htmlFor="roomType">Room Type</label>
                                    <RoomTypeSelect 
                                        onChange={this.handleRoomTypeChange}
                                        selectedValue={this.state.RoomType} />
                                </div>
                                <div className="row">
                                    <label htmlFor="projectGroup">Group</label>
                                    <GroupSelect
                                        onChange={this.handleGroupChange}
                                        ProjectId={this.state.ProjectId} 
                                        selectedValue={this.state.GroupId} />
                                </div>
                                <div className="row">
                                    <label htmlFor="roomLength">Room Length</label>
                                    <InputRange
                                        maxValue={20}
                                        minValue={1}
                                        value={this.state.RoomLength}
                                        onChange={RoomLength => this.setState({RoomLength})}
                                        name={"Room Length"}
                                        />
                                </div>
                                <div className="row">
                                    <p className="room-dimension-value">{this.state.RoomLength} meter(s)</p>
                                </div>
                                <div className="row">
                                    <label htmlFor="roomWidth">Room Width</label>
                                    <InputRange
                                            maxValue={20}
                                            minValue={1}
                                            value={this.state.RoomWidth}
                                            onChange={RoomWidth => this.setState({RoomWidth})}
                                            name={"Room Width"}
                                            />
                                </div>
                                <div className="row">
                                    <p className="room-dimension-value">{this.state.RoomWidth} meter(s)</p>
                                </div>
                                <div className="row">
                                    <label htmlFor="ceilingHeight">Ceiling Height</label>
                                    <InputRange
                                            maxValue={20}
                                            minValue={1}
                                            value={this.state.CeilingHeight}
                                            onChange={CeilingHeight => this.setState({CeilingHeight})}
                                            name={"Ceiling Height"}
                                            />
                                </div>
                                <div className="row">
                                    <p className="room-dimension-value">{this.state.CeilingHeight} meter(s)</p>
                                </div>
                                <div className="row">
                                    <div className="col-sm-6">
                                        <label htmlFor="numSensors">Number of Sensors</label>
                                        <InputRange
                                            maxValue={20}
                                            minValue={1}
                                            value={this.state.NumSensor}
                                            onChange={NumSensor => this.setState({NumSensor})}
                                            name={"Number of Sensors"}
                                            />
                                        <div className="row">
                                            <p className="room-dimension-value">{this.state.NumSensor}</p>
                                        </div>
                                    </div>
                                    <div className="col-sm-6">
                                        <label htmlFor="numKeypads">Number of Keypads</label>
                                        <InputRange
                                            maxValue={20}
                                            minValue={1}
                                            value={this.state.NumKeypads}
                                            onChange={NumKeypads => this.setState({NumKeypads})}
                                            name={"Number of Keypads"}
                                            />
                                        <div className="row">
                                            <p className="room-dimension-value">{this.state.NumKeypads}</p>
                                        </div>
                                    </div>
                                </div>                              
                            </div>
                            <div className="form-group color-side">
                                <div className="row">
                                    <h2>Environment Details</h2>
                                    <hr/>
                                </div>
                                <div className="row slider-row">
                                    <label htmlFor="ceilingColor">Ceiling Reflectivity</label>
                                    <CirclePicker
                                        colors={defaultColors}
                                        color={this.state.CeilingColorHex}
                                        onChangeComplete={this.handleCeilingColorChange} name="ceilingColor" />
                                </div>
                                <div className="row slider-row">
                                    <label htmlFor="wallColor">Wall Reflectivity</label>
                                    <CirclePicker
                                        colors={defaultColors}
                                        color={this.state.WallColorHex}
                                        onChangeComplete={this.handleWallColorChange} name="wallColor" />
                                </div>
                                <div className="row slider-row form-group">
                                    <label htmlFor="floorColor">Floor Reflectivity</label>
                                    <CirclePicker
                                        colors={defaultColors}
                                        color={this.state.FloorColorHex}
                                        onChangeComplete={this.handleFloorColorChange} name="floorColor" />
                                </div>
                                <div className="row">
                                    <h2>Luminaires</h2>
                                    <hr/>                                   
                                </div>   
                                <div className="row">
                                    { this.validateRequiredParams() != true &&
                                        <div className="alert alert-danger">
                                            Complete all fields before calculating Luminaire requirements
                                        </div>
                                    }

                                    { this.validateRequiredParams() == true &&
                                        <div>
                                            <div className="alert alert-success">
                                                {this.renderExistingLuminaireCount()}
                                            </div>

                                            <button type="button"
                                                className="btn btn-warning"
                                                onClick={() => {this.calculateLuminaires()}}>
                                                Calculate Luminaire Estimate
                                            </button>
                                        </div>
                                    }
                                </div>                      
                            </div>
                            <div className="button-row">                            
                                <div className="row">
                                    <button type="button"
                                        className="btn btn-danger pull-left"
                                        onClick={this.handleCancelClick}
                                        >
                                        Cancel
                                    </button>
                                    <button type="button"
                                        className="btn btn-success pull-right"
                                        onClick={this.handleSaveClick}
                                        // disabled={this.validateRequiredParams}
                                        >
                                            Save
                                    </button>
                                </div>
                            </div>      
                        </div>
                    </Modal>
            </div>
        );
    }

    renderLuminaireCount() {
        if (this.validateRequiredParams()) {
            this.calculateLuminaires();
            if (this.state.results && this.state.results.numLuminaires) {
                return (
                    <p>
                        Luminaires Required: {this.state.results.numLuminaires}
                    </p>
                );
            } else {
                return null;
            }
        } else {
            return null;
        }
    }

    checkSaveButtonDisabled() {
        if (this.state.NumLuminaires) {
            return false;
        }

        if (this.state.results && this.state.results.numLuminaires) {
            return false;
        }

        return true;
    }

    renderExistingLuminaireCount() {
        // Render the already saved number of luminaires for a room, unless we've
        // got a new result state, and thus we've calculated again, in which case
        // show that value
        if (this.state.NumLuminaires && !this.state.results) {
            return (
                <p>Estimated required Luminaires: {this.state.NumLuminaires}</p>
            );
        }

        if (this.state.results && this.state.results.numLuminaires) {
                return (
                    <p>Estimated required Luminaires: {this.state.results.numLuminaires}</p>
                )
        }

        return (
            <p>Waiting...</p>
        );
    }

    validateRequiredParams() {
        if (this.state.CeilingColorRgb == '') {
            return false;
        }

        if (this.state.WallColorRgb == '') {
            return false;
        }

        if (this.state.FloorColorRgb == '') {
            return false;
        }

        if (this.state.RoomLength == 0) {
            return false;
        }

        if (this.state.RoomWidth == 0) {
            return false;
        }

        if (this.state.CeilingHeight == 0) {
            return false;
        }

        if (this.state.RequiredLux == 0) {
            return false;
        }

        if (this.state.RoomName == '') {
            return false;
        }

        return true;
    }
}

export default EmptyRoom;