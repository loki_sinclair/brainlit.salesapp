import React from 'react';
import Api from '../common/api';

class GroupSelect extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            projectId: this.props.ProjectId,
            groups: [],
        };
    }

    async UNSAFE_componentWillMount() {
        const response = await Api.get('/projectgroups/' + this.state.projectId);
        this.setState({groups: response.data});
    }

    render() {
        return (
            <select name="roomType"
                className="form-control"
                placeholder="Select pre-defined group"
                onChange={this.props.onChange}
                value={this.props.selectedValue}
                >
                    <option key={0} value={"null"}>Select Group</option>
                    {this.state.groups.map((type) => {
                        return (<option key={type.ID} value={type.ID}>{type.Name}</option>);
                    })}
            </select>
        );
    }
}

export default GroupSelect;
