import React from 'react';
import {Link} from 'react-router-dom';
import ReactNotification from 'react-notifications-component';

import Api from '../common/api';

import EmptyRoom from '../components/emptyRoom';
import ProjectBlockList from '../components/ProjectBlockList';

import { library } from '@fortawesome/fontawesome-svg-core'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faCheck, faTimes, faCheckDouble, faPencilAlt,
    faTrashAlt, faBuilding, faMapMarkerAlt, faClone, faInfo } from '@fortawesome/free-solid-svg-icons'

library.add(faCheck, faTimes, faCheckDouble, faPencilAlt,
    faTrashAlt, faBuilding, faMapMarkerAlt, faClone);

import './listProject.css';
import 'react-notifications-component/dist/theme.css';

class ListProject extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            editingProject: false,
            selectedProject: null,
            roomCount: 0,
        };

        this.handleProjectNameChange = this.handleProjectNameChange.bind(this);
        this.handleProjectLocationChange = this.handleProjectLocationChange.bind(this);
        this.handleProjectTimezoneChange = this.handleProjectTimezoneChange.bind(this);
        this.handleProjectCoordsChange = this.handleProjectCoordsChange.bind(this);
        this.handleProjectFacilityTypeChange = this.handleProjectFacilityTypeChange.bind(this);

        this.handleCreateRoom = this.handleCreateRoom.bind(this);

        this.handleEditClick = this.handleEditClick.bind(this);
        this.handleDeleteClick = this.handleDeleteClick.bind(this);

        this.handleProjectSave = this.handleProjectSave.bind(this);

        this.handleDeclineProjectClick = this.handleDeclineProjectClick.bind(this);
        this.handleApproveProjectClick = this.handleApproveProjectClick.bind(this);

        this.addNotification = this.addNotification.bind(this);
        this.notificationDOMRef = React.createRef();
    }

    addNotification(title, message, type) {
        this.notificationDOMRef.current.addNotification({
            title: title,
            message: message,
            type: type,
            insert: 'top',
            container: 'top-right',
            animationIn: ['animated', 'fadeIn'],
            animationOut: ['animated', 'fadeOut'],
            dismiss: {duration: 2000},
            dismissable: {click: true}
        });
    }

    renderRooms() {
        if (this.state.selectedProject.rooms != null) {
            if (this.state.selectedProject.rooms.length > 0) {
                return (this.state.selectedProject.rooms.map((room) => {
                        return (                     
                            <EmptyRoom room={room} key={room.id} projectId={this.state.selectedProject.ID} />
                        );
                    })
                );
            }
        }
    }

    renderProjectBlocks() {
        return (
            <ProjectBlockList
                Project={this.state.selectedProject} />
        );
    }

    async handleDeclineProjectClick() {
        const response = await Api.post('/projects/decline/' +
            this.state.selectedProject.ID);
        this.addNotification('Complete!', 'Project ' + this.state.selectedProject.Name +
            ' has been declined', 'success');
    }

    async handleApproveProjectClick() {
        const response = await Api.post('/projects/approve/' +
            this.state.selectedProject.ID);
        this.addNotification('Complete!', 'Project ' + this.state.selectedProject.Name +
            ' has been approved', 'success');
    }

    handleCreateRoom() {
        var proj = this.state.selectedProject;
        if (proj.rooms == null) {
            proj.rooms = [];
        }

        var room = {
            id: 0,
            name: 'Room ' + (this.state.roomCount + 1),
            roomType: 0,
            projectId: proj.ID,
            groupId: 0,
            roomLength: 1,
            roomWidth: 1,
            ceilingHeight: 1,
            wallColor: '#fff',
            wallColorRgb: '255,255,255',
            ceilingColor: '#fff',
            ceilingColorRgb: '255,255,255',
            floorColor: '#fff',
            floorColorRgb: '255,255,255',
            numSensor: 1,
            numKeypads: 1,
            numLuminaires: 0,
        }

        proj.rooms.push(room);

        this.setState({selectedProject: proj,
            roomCount: this.state.roomCount+= 1});
    }

    handleProjectNameChange(event) {
        var proj = this.state.selectedProject;
        proj.Name = event.target.value;

        this.setState({selectedProject: proj});
    }

    handleProjectLocationChange(event) {
        var proj = this.state.selectedProject;
        proj.Location = event.target.value;

        this.setState({selectedProject: proj});
    }

    handleProjectTimezoneChange(event) {
        var proj = this.state.selectedProject;
        proj.Timezone = event.target.value;

        this.setState({selectedProject: proj});
    }

    handleProjectCoordsChange(event) {
        var proj = this.state.selectedProject;
        proj.Coords = event.target.value;

        this.setState({selectedProject: proj});
    }
    
    handleProjectFacilityTypeChange(event) {
        var proj = this.state.selectedProject;
        proj.FacilityType = event.target.value;

        this.setState({selectedProject: proj});
    }

    handleProjectSave() {
        console.log('here');
    }

    handleEditClick(proj) {
        this.setState(
            {
                editingProject: true,
                selectedProject: proj,
            }
        );
    }

    async handleDeleteClick(id) {
        const response = await Api.delete('/projects/' + id);
        this.props.reloadProjects();
        this.addNotification('Completed!', 'Project Deleted', 'success');
    }

    render() {
        if (!this.state.editingProject) {
            return (
                <div className="container">
                    <h3>Projects</h3>
                    <div className="alert alert-info">
                        <p>
                            Existing projects are listed below. If you are yet to create a project. You can do so <a 
                                className="alert-link link-button"
                                onClick={this.props.handleClick}
                                >here</a>
                        </p>
                    </div>

                    <div className="panel panel-default">
                        <div className="panel-heading">Existing Projects</div>
                            <div className="row">
                                {
                                    this.props.projects && this.props.projects.length > 0 && this.props.projects.map((proj) => {
                                        return (
                                            <div className="col-sm-6 col-md-4" key={proj.ID}>
                                                <div className={(proj.VerifiedByCustomer == 1 && proj.VerifiedByBL == 1 ? 'thumbnail project-ready' : 'thumbnail')}>
                                                    <center><FontAwesomeIcon icon="building" className="project-icon"/></center>
                                                    <div className="caption text-center">
                                                        <h3>{proj.Name}</h3>
                                                    </div>
                                                    <p>
                                                        <span className="location-name">
                                                            <FontAwesomeIcon icon="map-marker-alt" className="map-icon"/>&nbsp;{(proj.Location != "" ? proj.Location : "...unknown")}
                                                        </span>
                                                    </p>
                                                    <p>
                                                        <span className="validation-state">
                                                        Customer Verfied: {(proj.VerifiedByCustomer == 1 ? <FontAwesomeIcon icon="check" /> : <FontAwesomeIcon icon="times" className="red"/>)}
                                                        </span>
                                                        <span className="validation-state">
                                                            BrainLit Verified: {(proj.VerifiedByBL == 1 ? <FontAwesomeIcon icon="check-double" /> : <FontAwesomeIcon icon="times" className="red"/>)}
                                                        </span>
                                                    </p>
                                                    <p>
                                                        <a className="btn btn-success btn-lg" onClick={() => this.handleEditClick(proj)}>Edit</a>
                                                        <a className="btn btn-danger btn-lg" onClick={() => this.handleDeleteClick(proj.ID)}>Delete</a>
                                                    </p>
                                                </div>
                                            </div>
                                        );
                                    })
                                }
                            </div>
                    </div>
                    <ReactNotification ref={this.notificationDOMRef} />
                </div>
            );
        } else {
            return (
                <div className="container">
                    <div className="better-padding">
                        <h3>Project: {this.state.selectedProject.Name}</h3>
                    </div>
                    <div className="better-padding">
                        {this.renderConfirmationTools()}
                    </div>
                    <br/>
                    <div className="better-padding">
                        { this.renderProjectBlocks() }
                    </div>

                    <div className="better-padding">
                        { this.renderRooms() }
                    </div>
                    <ReactNotification ref={this.notificationDOMRef} />
                </div>
            );
        }
    }

    renderConfirmationTools() {
        return (
            <span>
                <button type="button" className="btn btn-danger pull-left" onClick={this.handleDeclineProjectClick}>Decline Project</button>
                <button type="button" className="btn btn-success pull-right" onClick={this.handleApproveProjectClick}>Approve Project</button>
            </span>
        );
    }
}

export default ListProject;