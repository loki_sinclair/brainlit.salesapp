import React from 'react';
import Axios from 'axios';

import './login.css';

class Login extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            loggedIn: this.props.loggedIn,
            username: '',
            password: '',
            token: ''
        };

        this.handleUsernameChange = this.handleUsernameChange.bind(this);
        this.handlePasswordChange = this.handlePasswordChange.bind(this);
        this.handleLogin = this.handleLogin.bind(this);
    }

    handleUsernameChange(event) {
        this.setState({username: event.target.value});
    }

    handlePasswordChange(event) {
        this.setState({password: event.target.value});
    }

    handleLogin() {
        this.props.login(this.state.username, this.state.password);
    }

    render() {
        return (
            <div className="container login-box">
                <div className="form-group">
                    <label htmlFor="exampleInputEmail1">Username</label>
                    <input type="text" className="form-control" id="username"
                        name="username"
                        placeholder="Username/Email"
                        onChange={this.handleUsernameChange} />
                </div>
                <div className="form-group">
                    <label htmlFor="exampleInputPassword1">Password</label>
                    <input type="password" className="form-control" id="password"
                        name="password"
                        placeholder="Password" 
                        onChange={this.handlePasswordChange}/>
                </div>
                <button type="button" className="btn btn-primary" onClick={this.handleLogin}>Submit</button>
            </div>
        );
    }
};

export default Login;