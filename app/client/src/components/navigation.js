import React from 'react';

class Navigation extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div>
                <nav className="navbar navbar-inverse navbar-static-top">
                    <div className="container-fluid">
                        <div className="navbar-header">
                            <a className="navbar-brand" href="/">BrainLit</a>
                        </div>

                        <div className="collapse navbar-collapse">
                            <ul className="nav navbar-nav">
                                <li><a href="/">Home</a></li>
                                <li><a href="/projects">Projects</a></li>
                                <li><a href="/calculator">Luminaire Calculator</a></li>
                                {/* <li className="nav-item nav-link" href="/technical">Technical Information</li>
                                <li className="nav-item nav-link" href="/marketing">Marketing Information</li> */}
                            </ul>
                            <p className="navbar-text navbar-right"><a href="/logout" className="navbar-link">Logout</a></p>
                        </div>
                    </div>
                </nav>
            </div>
        );
    }
};

export default Navigation;