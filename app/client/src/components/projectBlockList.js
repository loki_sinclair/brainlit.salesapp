import React from 'react';
import Moment from 'react-moment';

import BlockDetails from './blockDetails';
import CreateBlock from './createBlock';
import CreateGroup from './createGroup';

//import Api from '../common/api';

import { library } from '@fortawesome/fontawesome-svg-core'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faClone, faTrashAlt } from '@fortawesome/free-solid-svg-icons'

library.add(faClone, faTrashAlt);

import './projectBlockList.css';
import Api from '../common/api';

const defaultDateFormat = 'MMM Do YY';

class ProjectBlockList extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            projectBlocks: [],
            projectId: this.props.Project.ID,
            displayBlockDetails: false,
            selectedBlock: null,
        };

        this.handleBlockClick = this.handleBlockClick.bind(this);
        this.loadProjectBlocks = this.loadProjectBlocks.bind(this);
        this.handleDeleteClick = this.handleDeleteClick.bind(this);
    }

    getProjectBlock(inBlock) {
        for (let i = 0; i < this.state.projectBlocks.length; i++) {
            if (this.state.projectBlocks[i].ID == inBlock.ID) {
                return this.state.projectBlocks[i];
            }
        }
    }

    removeDeletedBlock(id) {
        this.setState({projectBlocks: this.state.projectBlocks.filter((block) =>{
            if (block.ID != id) {
                return block;
            }
        })});
    }

    async handleDeleteClick(id) {
        await Api.delete('/projectblocks/' + id);
        this.removeDeletedBlock(id);    
    }

    async handleBlockClick(block) {
        this.setState({displayBlockDetails: false, selectedBlock: null});
        const response = await Api.get('/projectblocks/' + block.ID + '/rooms');
        block.rooms = response.data;
        this.selectBlock(block);
    }

    selectBlock(block) {
        this.setState({
            displayBlockDetails: true,
            selectedBlock: block,
        });
    }

    UNSAFE_componentWillMount() {
        this.loadProjectBlocks(this.state.projectId);
    }

    async loadProjectBlocks(projectId) {
        const response = await Api.get('/projectblocks/' + projectId);
        this.setState({projectBlocks: response.data});
    }

    render() {
        return (
            <div className="container">
                <div>
                    <div>
                        <br/>
                        <h3>Create Block</h3>
                        <div className="alert alert-warning">
                            <strong>What is a Block?</strong>
                            <p>
                                Blocks make up your entire project. Let's think in terms of Appartment buildings: In this situation,
                                a Block would be an entire <strong>Floor</strong>
                            </p>
                        </div>
                    </div>
                    <div>
                        {this.renderBlockCreate()}
                    </div>
                    <div>
                        <h3>Create Group</h3>
                        <div className="alert alert-warning">
                            <strong>What is a Group?</strong>
                            <p>
                                Groups make up the components of a Block. Using the Appartment building analogy again, you can
                                think of Groups, as the individual Appartments.
                            </p>
                        </div>
                    </div>
                    <div>
                        {this.renderGroupCreate()}
                    </div>
                </div>
                <div>
                    <div className="section-header">
                        <h3>Your existing Blocks</h3>
                    </div>
                </div>
                <div className="nav-side">
                    <ul className="list-group">
                        {this.renderProjectBlocks()}
                    </ul>                    
                </div>
                <div className="content-side">
                    <div className="section-header">
                        <h3>
                            { this.state.selectedBlock != null && 
                                <p>Rooms for {this.state.selectedBlock.Name}</p>
                            }

                            { this.state.selectedBlock == null &&
                                <p>Rooms</p>
                            }
                        </h3>
                    </div>
                    {this.renderBlockDetails()}
                </div>
            </div>
        );
    }

    renderBlockCreate() {
        return (
            <CreateBlock ProjectId={this.state.projectId} ReloadBlocks={this.loadProjectBlocks}/>
        );
    }

    renderGroupCreate() {
        return (
            <CreateGroup ProjectId={this.state.projectId} />
        );
    }

    renderProjectBlocks() {
        if (this.state.projectBlocks && this.state.projectBlocks.length > 0) {
            return (
                this.state.projectBlocks.map((block) => {
                    return (
                        <button type="button" className="list-group-item clearfix"
                            onClick={() => {this.handleBlockClick(block)}}>
                            <h4 className="list-group-item-heading">
                                <FontAwesomeIcon icon="clone" className="block-icon"/>
                                {block.Name}
                            </h4>
                            <h4 className="list-group-item-text">
                                <small>Created: <Moment format={defaultDateFormat}>{block.CreatedAt}</Moment></small>
                            </h4>
                        </button>
                    );
                })
            );
        } else {
            return (
                <p>You haven't set up any Project Blocks yet.</p>
            );
        }
    }

    renderBlockDetails() {
        if (this.state.displayBlockDetails) {
            return (
                <div>
                    <BlockDetails Block={this.state.selectedBlock} />
                </div>
            );
        } else {
            return (
                <div className="rooms-block-text">
                    <h1><small>Rooms assigned to Blocks will display upon selecting a Block.</small></h1>
                </div>
            );
        }
    }
};

export default ProjectBlockList;