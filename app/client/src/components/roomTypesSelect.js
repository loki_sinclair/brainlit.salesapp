import React from 'react';
import Api from '../common/api';

class RoomTypeSelect extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            roomTypes: [],
        };
    }

    async UNSAFE_componentWillMount() {
        const response = await Api.get('/roomtypes');
        this.setState({roomTypes: response.data});
    }

    render() {
        return (
            <select name="roomType"
                className="form-control"
                placeholder="Select pre-defined room type"
                onChange={this.props.onChange}
                value={this.props.selectedValue}
                >
                    <option key={0} value={"null"}>Select Room Type</option>
                    {this.state.roomTypes.map((type) => {
                        return (<option key={type.ID} value={type.ID}>{type.Name}</option>);
                    })}
            </select>
        );
    }
};

export default RoomTypeSelect;