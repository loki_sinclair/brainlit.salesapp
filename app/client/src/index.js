
import React from "react";
import ReactDOM from "react-dom";

import AppRouter from './routes';
import Navigation from './components/navigation';
import Login from './components/login';
import Api from "./common/api";

class App extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            isLoggedIn: false,
            token: ''
        };

        this.handleLogin = this.handleLogin.bind(this);
    }

    async handleLogin(username, password) {
        const response = await Api.post('/login', {
            'username': username,
            'password': password,
        });

        localStorage.setItem('token', response.token);
        this.setState({isLoggedIn: (response.token != null), token: response.token});
    }

    isLoggedIn() {
        return localStorage.getItem("token") != null;
    }

    render() {
        if (!this.isLoggedIn()) {
            return (
                <div className="container">
                    <Login login={this.handleLogin} />
                </div>
            )
        } else {
            return (
                <div className="containter">
                    <Navigation loggedIn={this.isLoggedIn} />
                    <AppRouter loggedIn={this.isLoggedIn} />
                </div>
            );
        }
    }
}

export default App;

ReactDOM.render(
    <App />,
    document.getElementById('app')
);