import React from 'react';
import { ChromePicker } from 'react-color';

import InputRange from 'react-input-range';

import 'react-input-range/lib/css/index.css';
import './calculator.css';
import Api from '../../common/api';

class Calculator extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            ceilingColor: 'ffff',
            ceilingColorRGB: '',
            floorColor: '#fff',
            floorColorRGB: '',
            wallColor: '#fff',
            wallColorRGB: '',
            roomLength: 9,
            roomWidth: 6,
            ceilingHeight: 3.5,
            requiredLux: 1078,
            results: null
        }

        this.handleCalculate = this.handleCalculate.bind(this);
    }

    handleCeilingColorChange = (color, event) => {
        this.setState({ceilingColor: color.rgb, 
            ceilingColorRGB: color.rgb.r + "," + color.rgb.g + "," + color.rgb.b});
    };

    handleFloorColorChange = (color, event) => {
        this.setState({floorColor: color.rgb, 
            floorColorRGB: color.rgb.r + "," + color.rgb.g + "," + color.rgb.b})
    }

    handleWallColorChange = (color, event) => {
        this.setState({wallColor: color.rgb, 
            wallColorRGB: color.rgb.r + "," + color.rgb.g + "," + color.rgb.b})
    }

    handleLengthSliderChange = (value) => {
        this.setState({roomLength: value});
    }

    handleLuxChange = (event) => {
        this.setState({requiredLux: parseInt(event.target.value)});
    }

    async handleCalculate() {
        const response = await Api.post('/calculate_luminaires', {
            ceilingColor: this.state.ceilingColorRGB,
            wallColor: this.state.wallColorRGB,
            floorColor: this.state.floorColorRGB,
            roomLength: this.state.roomLength,
            roomWidth: this.state.roomWidth,
            ceilingHeight: this.state.ceilingHeight,
            requiredLux: this.state.requiredLux,            
        });
        this.setState({results: response.data});
    }

    render() {
        return (
            <div className="container">
                <div className="detail-side">                
                    <div className="panel panel-primary">
                        <div className="panel-heading">
                            <h3 className="panel-title">Environment Colors</h3>
                        </div>
                        <div className="panel-body">
                            <div className="alert alert-default">
                                Choose the best match of colors for Ceiling, Walls and Floor
                            </div>
                            <div className="row">
                                <div className="col-xs-6 col-sm-4">
                                    <div className="row text-center">
                                        <p className="bolded-text-muted">
                                            Ceiling Color
                                        </p>
                                    </div>
                                    <div className="row">
                                        <center>
                                            <ChromePicker color={this.state.ceilingColor} onChangeComplete={this.handleCeilingColorChange} />
                                        </center>
                                    </div>
                                </div>
                                <div className="col-xs-6 col-sm-4">
                                    <div className="row text-center">                            
                                        <p className="bolded-text-muted">Wall Color</p>
                                    </div>
                                    <div className="row">
                                        <center>
                                            <ChromePicker color={this.state.wallColor} onChangeComplete={this.handleWallColorChange} />
                                        </center>
                                    </div>
                                </div>
                                <div className="col-xs-6 col-sm-4">
                                    <div className="row text-center">
                                        <p className="bolded-text-muted">Floor Color</p>
                                    </div>
                                    <div className="row">
                                        <center>
                                            <ChromePicker color={this.state.floorColor} onChangeComplete={this.handleFloorColorChange} />
                                        </center>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <br/>
                    <div className="panel panel-primary">
                        <div className="panel-heading">
                            <h3 className="panel-title">About the Room</h3>
                        </div>
                        <div className="panel-body">
                            <div className="alert alert-default">
                                Tell us about the dimensions of the room
                            </div>

                            <div className="row">
                                <div className="container">
                                    <p className="bolded-text">Room Length</p>
                                </div>
                            </div>
                            <div className="row">
                                <div className="col-xs-6 col-sm4">
                                    <InputRange
                                        maxValue={20}
                                        minValue={1}
                                        value={this.state.roomLength}
                                        onChange={roomLength => this.setState({roomLength})}
                                        name={"Room Length"}
                                        />                            
                                </div>
                                <div className="col-xs-6 col-sm4">
                                    <p className="room-dimension-value">{this.state.roomLength} meter(s)</p>
                                </div>
                            </div>
                            <div className="row">
                                <div className="container">
                                    <p className="bolded-text">Room Width</p>
                                </div>
                            </div>
                            <div className="row">
                                <div className="col-xs-6 col-sm4">
                                    <InputRange
                                        maxValue={20}
                                        minValue={1}
                                        value={this.state.roomWidth}
                                        onChange={roomWidth => this.setState({roomWidth})}
                                        name={"Room Width"}
                                        />
                                </div>                        
                                <div className="col-xs-6 col-sm4">
                                    <p className="room-dimension-value">{this.state.roomWidth} meter(s)</p>
                                </div>
                            </div>
                            <div className="row">
                                <div className="container">
                                    <p className="bolded-text">Ceiling Height</p>
                                </div>
                            </div>
                            <div className="row">
                                <div className="col-xs-6 col-sm4">
                                    <InputRange
                                        maxValue={20}
                                        minValue={1}
                                        step={0.5}
                                        value={this.state.ceilingHeight}
                                        onChange={ceilingHeight => this.setState({ceilingHeight})}
                                        name={"Ceiling Height"}
                                        />
                                </div>                        
                                <div className="col-xs-6 col-sm4">
                                    <p className="room-dimension-value">{this.state.ceilingHeight} meter(s)</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <br/>
                    <div className="panel panel-primary">
                        <div className="panel-heading">
                            <h3 className="panel-title">Luminance</h3>
                        </div>
                        <div className="panel-body">
                            <div className="alert alert-default">
                                Set the required luminance
                            </div>

                            <div className="row">
                                <div className="container">
                                    <div className="col-xs-6 col-sm4">
                                        <p className="bolded-text">Required Luminance</p>
                                    </div>
                                    <div className="col-xs-6 col-sm4">
                                        <input
                                            className="input-group" 
                                            type="text"
                                            value={this.state.requiredLux}
                                            onChange={this.handleLuxChange}
                                            />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div className="row">
                        <button type="button" className="btn btn-success calculate-button" onClick={this.handleCalculate}>Calculate</button>
                    </div>
                </div>
                <div className="result-side">
                    <div className="panel panel-success result-box">
                        <div className="panel-heading">
                            <h3 className="panel-title">Results</h3>
                        </div>
                        <div className="panel-body">
                        { this.state.results == null &&
                            <div className="alert alert-warning">
                                Waiting...
                            </div>
                        }

                        { this.state.results != null &&
                            <div>
                                <p>Ceiling Reflectivity: {this.state.results.ceilingReflectivity}</p>
                                <p>Wall Reflectivity: {this.state.results.wallReflectivity}</p>
                                <p>Floor Reflectivity: {this.state.results.floorReflectivity}</p>
                                <p><br/></p>
                                <p>Luminaires Required: {this.state.results.numLuminaires}</p>
                            </div>
                        }
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default Calculator;