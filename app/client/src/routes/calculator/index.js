import React from 'react';
import Loadable from 'react-loadable';

import Loading from '../../components/loading';

const LoadableComponent = Loadable({
    loader: () => import('./calculator'),
    loading: Loading,
});

const LoadableCalculator = () => <LoadableComponent />

export default LoadableCalculator;