import React from 'react';
import Loadable from 'react-loadable';

import Loading from '../../components/loading';

const LoadableComponent = Loadable({
    loader: () => import('./editProject'),
    loading: Loading,
});

const LoadableEditProject = () => <LoadableComponent />

export default LoadableEditProject;