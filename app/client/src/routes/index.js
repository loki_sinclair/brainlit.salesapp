import React from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';

import Home from './home';
import Tools from './tools';
import NotFound from './notFound';
import Calculator from './calculator';
import EditProject from './editProject';

class AppRouter extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <Router>
                <Switch>                
                    <Route exact path='/' component={Home} />
                    { this.props.loggedIn() &&
                        <div>
                            <Route exact path='/projects' component={Tools} />
                            <Route path='/project/:project_id' component={EditProject} />
                            <Route exact path='/calculator' component={Calculator} />
                        </div>
                    }                
                    <Route exact component={NotFound} />
                </Switch>
            </Router>
        );
    }
};

export default AppRouter;