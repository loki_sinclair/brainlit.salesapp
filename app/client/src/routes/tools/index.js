import React from 'react';
import Loadable from 'react-loadable';

import Loading from '../../components/loading';

const LoadableComponent = Loadable({
    loader: () => import('./tools'),
    loading: Loading,
});

const LoadableTools = () => <LoadableComponent />

export default LoadableTools;