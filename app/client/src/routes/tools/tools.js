import React from 'react';

import CreateProject from '../../components/createProject';
import ListProject from '../../components/listProject';

import './tools.css';
import Api from '../../common/api';

class Tools extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            creatingProject: false,
            projects: null
        }

        this.handleCreateNewProject = this.handleCreateNewProject.bind(this);
    }

    handleCreateNewProject() {
        this.setState({creatingProject: true});
    }

    handleProjectReload() {
        this.loadProjects();
    }

    componentWillMount() {
        this.loadProjects();
    }

    async loadProjects() {
        const response = await Api.get('/projects');
        this.setState({projects: response.data.projects});
    }

    render() {
        if (this.state.creatingProject) {
            return (
                <CreateProject />
            );
        } else {
            return (
                <div>
                    <ListProject
                        projects={this.state.projects}
                        handleClick={this.handleCreateNewProject}
                        reloadProjects={() => this.handleProjectReload()}
                        />
                </div>
            );
        }
    }
};

export default Tools;