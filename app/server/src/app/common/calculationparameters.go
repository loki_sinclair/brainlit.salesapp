package common

import (
	"errors"
	"fmt"
	"strconv"
	"strings"
)

// CalculationParameters object to hold values for luminaire requirement calculation
type CalculationParameters struct {
	CoefficientsA     []CoefficientA
	CoefficientsB     []CoefficientB
	CoefficientsC     []CoefficientC
	CoefficientsR     []CoefficientR
	CeilingColor      string
	WallColor         string
	FloorColor        string
	RoomLength        float32
	RoomWidth         float32
	CeilingHeight     float32
	RequiredLuminance int
	CeilingColorRGB   []float64
	WallColorRGB      []float64
	FloorColorRGB     []float64
}

// ConvertCeilingRGB Converts an RGB string into its individual R G B values
func (cp *CalculationParameters) ConvertCeilingRGB(rgb string) {
	r, g, b, err := splitRGB(rgb)
	if err != nil {
		panic(err)
	}
	cp.CeilingColorRGB = []float64{
		float64(r / 255.0),
		float64(g / 255.0),
		float64(b / 255.0),
	}
}

// ConvertWallRGB Converts an RGB string into its individual R G B values
func (cp *CalculationParameters) ConvertWallRGB(rgb string) {
	r, g, b, err := splitRGB(rgb)
	if err != nil {
		panic(err)
	}
	cp.WallColorRGB = []float64{
		float64(r / 255.0),
		float64(g / 255.0),
		float64(b / 255.0),
	}
}

// ConvertFloorRGB Converts an RGB string into its individual R G B values
func (cp *CalculationParameters) ConvertFloorRGB(rgb string) {
	r, g, b, err := splitRGB(rgb)
	if err != nil {
		panic(err)
	}
	cp.FloorColorRGB = []float64{
		float64(r / 255.0),
		float64(g / 255.0),
		float64(b / 255.0),
	}
}

func splitRGB(rgb string) (int, int, int, error) {
	parts := strings.Split(rgb, ",")
	if len(parts) == 3 {
		r, err := strconv.Atoi(parts[0])
		if err != nil {
			return 0, 0, 0, err
		}
		g, err := strconv.Atoi(parts[1])
		if err != nil {
			return 0, 0, 0, err
		}
		b, err := strconv.Atoi(parts[2])
		if err != nil {
			return 0, 0, 0, err
		}

		return r, g, b, nil
	}

	fmt.Println(fmt.Sprintf("Got %s and %s", rgb, parts))
	return 0, 0, 0, errors.New("Unable to split RGB")
}
