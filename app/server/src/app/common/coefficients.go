package common

// CoefficientR Contains coefficient values for calculating luminaire quantity
type CoefficientR struct {
	Id    int
	Name  string
	Value float64
}

// CoefficientA Contains coefficient values for calculating luminaire quantity
type CoefficientA struct {
	Id    int
	Name  string
	Value float64
}

// CoefficientB Contains coefficient values for calculating luminaire quantity
type CoefficientB struct {
	Id    int
	Name  string
	Value float64
}

// CoefficientC Contains coefficient values for calculating luminaire quantity
type CoefficientC struct {
	Id    int
	Name  string
	Value float64
}
