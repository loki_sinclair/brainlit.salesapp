package common

import (
	"database/sql"
	"fmt"
	"os"
	"path/filepath"
	"strconv"
	"sync"

	// Custom import for replacement mysql driver
	_ "github.com/go-sql-driver/mysql"
	"github.com/joho/godotenv"
	migrate "github.com/rubenv/sql-migrate"
)

var (
	instance = &MysqlDB{&sql.DB{}, sync.Once{}}
)

// MysqlDB struct contains active singleton instanced connection
// to the database
type MysqlDB struct {
	Connection *sql.DB
	once       sync.Once
}

// Database definition of the database object
type Database struct {
	Host     string
	Port     int
	Username string
	Password string
	Name     string
	MysqlFix string
}

// GetDBInstance Returns a singleton instance of the database connection
func GetDBInstance(envfile ...string) *MysqlDB {
	instance.once.Do(func() {
		if err := godotenv.Load(envfile...); err != nil {
			panic(err)
		}
		port, err := strconv.Atoi(os.Getenv("DB_PORT"))
		if err != nil {
			panic(err)
		}
		db := Database{
			Host:     os.Getenv("DB_HOST"),
			Port:     port,
			Username: os.Getenv("DB_USERNAME"),
			Password: os.Getenv("DB_PASSWORD"),
			Name:     os.Getenv("DB_NAME"),
			MysqlFix: "parseTime=true",
		}
		dsn := fmt.Sprintf("%s:%s@tcp(%s:%d)/%s?%s", db.Username, db.Password,
			db.Host, db.Port, db.Name, db.MysqlFix)
		fmt.Printf("Setting DSN: %s\n", dsn)
		session, err := sql.Open("mysql", dsn)
		if err != nil {
			panic(err)
		}
		instance.Connection = session
	})

	return instance
}

// Close Closes the database connection
func (d *Database) Close(database *sql.DB) {
	database.Close()
}

// LoadProjects Loads project list based upon user id
func (d *Database) LoadProjects(user *User) ([]Project, error) {
	db := GetDBInstance()
	var projects []Project
	stmt, err := db.Connection.Prepare("SELECT * FROM project WHERE user_id = ?")
	if err != nil {
		panic(err)
	}
	defer stmt.Close()
	rows, err := stmt.Query(user.Id)
	if err != nil {
		panic(err)
	}
	defer rows.Close()
	for rows.Next() {
		var p Project
		err := rows.Scan(&p.ID, &p.Name, &p.Location, &p.Timezone, &p.CustomerID,
			&p.Coords, &p.FacilityType, &p.VerifiedByCustomer, &p.VerifiedByBL,
			&p.UserID)
		if err = rows.Err(); err != nil {
			panic(err)
		}
		projects = append(projects, p)
	}

	return projects, nil
}

// MigrateAll Migrates any sql files in database/migrate to up/down the database
func (d *Database) MigrateAll() (bool, error) {
	db := GetDBInstance()
	path, err := filepath.Abs("./")
	if err != nil {
		panic(err)
	}

	migrations := &migrate.FileMigrationSource{
		Dir: path + "/database/migrations/migrate",
	}

	n, err := migrate.Exec(db.Connection, "mysql", migrations, migrate.Up)
	if err != nil {
		return false, err
	}

	fmt.Printf("Applied %d database migrations!\n", n)
	return true, nil
}

// MigrateDown Rollsback any sql migrations that have been run on the database
func (d *Database) MigrateDown() (bool, error) {
	db := GetDBInstance()
	path, err := filepath.Abs("./")
	if err != nil {
		panic(err)
	}
	migrations := &migrate.FileMigrationSource{
		Dir: path + "/database/migrations/migrate",
	}

	n, err := migrate.Exec(db.Connection, "mysql", migrations, migrate.Down)
	if err != nil {
		return false, err
	}

	fmt.Printf("Rolledback %d database migrations!\n", n)
	return true, nil
}
