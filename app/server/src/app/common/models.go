package common

import "time"

type Customer struct {
	ID             int
	Name           string
	ContactEmail   string
	SalesContactID int
}

type Project struct {
	ID                 int
	Name               string
	Location           string
	Timezone           string
	CustomerID         int
	Coords             string
	FacilityType       int
	VerifiedByCustomer int
	VerifiedByBL       int
	UserID             int
}

type ProjectBlock struct {
	ID        int
	ProjectID int
	Name      string
	CreatedAt time.Time
	UpdatedAt time.Time
	DeletedAt time.Time
}

type ProjectGroup struct {
	ID        int
	ProjectID int
	Name      string
	CreatedAt time.Time
	UpdatedAt time.Time
	DeletedAt time.Time
}

type RoomGroup struct {
	ID        int
	Name      string
	CreatedAt time.Time
	UpdatedAt time.Time
	DeletedAt time.Time
}

type Room struct {
	ID              int        `json:"id"`
	Name            string     `json:"name"`
	RoomType        *int       `json:"roomType"`
	CreatedAt       time.Time  `json:"created_at"`
	UpdatedAt       time.Time  `json:"updated_at"`
	DeletedAt       *time.Time `json:"deleted_at"`
	GroupId         *int       `json:"groupId"`
	BlockId         *int       `json:"blockId"`
	ProjectId       *int       `json:"projectId"`
	RoomLength      *float64   `json:"roomLength"`
	RoomWidth       *float64   `json:"roomWidth"`
	CeilingHeight   *float64   `json:"ceilingHeight"`
	CeilingColorHex string     `json:"ceilingColor"`
	WallColorHex    string     `json:"wallColor"`
	FloorColorHex   string     `json:"floorColor"`
	CeilingColorRgb string     `json:"ceilingColorRgb"`
	WallColorRgb    string     `json:"wallColorRgb"`
	FloorColorRgb   string     `json:"floorColorRgb"`
	NumSensor       *int       `json:"numSensor"`
	NumKeypads      *int       `json:"numKeypads"`
	NumLuminaires   *int       `json:"numLuminaires"`
}

type RoomType struct {
	ID        int
	Name      string
	CreatedAt time.Time
	UpdatedAt time.Time
	DeletedAt time.Time
}

type RoomList struct {
	Room     *Room
	RoomType *RoomType
}
