package common

var token string

// User Represents a user object
type User struct {
	Id        int
	Username  string
	Password  string
	Perms     int
	CreatedAt string
	UpdatedAt string
	DeletedAt string
}

func (u *User) SetToken(jwtToken string) {
	token = jwtToken
}

func (u *User) GetToken() string {
	return token
}
