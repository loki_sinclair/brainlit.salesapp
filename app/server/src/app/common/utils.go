package common

import (
	"encoding/json"

	"github.com/Knetic/govaluate"
	"github.com/gin-gonic/gin"
)

// Evalutate Evaluates a mathematical formula using govaluate
func Evaluate(formula string, params map[string]interface{}) float64 {
	expression, err := govaluate.NewEvaluableExpression(formula)
	if err != nil {
		panic(err)
	}
	result, err := expression.Evaluate(params)
	if err != nil {
		panic(err)
	}

	return result.(float64)
}

func GetRequestBodyAsMap(c *gin.Context) map[string]interface{} {
	var retData map[string]interface{}
	data, err := c.GetRawData()
	if err != nil {
		panic(err)
	}
	err = json.Unmarshal(data, &retData)
	if err != nil {
		panic(err)
	}
	return retData
}
