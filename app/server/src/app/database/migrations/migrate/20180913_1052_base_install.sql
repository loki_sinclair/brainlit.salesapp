--
-- Default BLSales app sql installation
--

-- +migrate Up
 CREATE TABLE `blsales`.`users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(64) NOT NULL,
  `password` varchar(64) NOT NULL,
  `perms` varchar(45) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
);

CREATE TABLE `blsales`.`room` (
	`id` INT NOT NULL AUTO_INCREMENT,
	`name` varchar(64) NOT NULL DEFAULT 'Unnamed Room',
	`room_type` INT NOT NULL DEFAULT '1',
	`created_at` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
	`updated_at` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
	`deleted_at` DATETIME,
	`group_id` INT NOT NULL,
	PRIMARY KEY (`id`)
);

CREATE TABLE `blsales`.`room_types` (
	`id` INT NOT NULL AUTO_INCREMENT,
	`name` varchar(64) NOT NULL DEFAULT '',
	`created_at` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
	`updated_at` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
	`deleted_at` DATETIME,
	PRIMARY KEY (`id`)
);

CREATE TABLE `blsales`.`room_metrics` (
	`room_type_id` INT NOT NULL,
	`name` varchar(64) NOT NULL UNIQUE,
	`value` varchar(64),
	`created_at` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
	`updated_at` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
	`deleted_at` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP
);

CREATE TABLE `blsales`.`room_group` (
	`id` INT NOT NULL AUTO_INCREMENT,
	`name` varchar(64) NOT NULL,
	`created_at` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
	`updated_at` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
	`deleted_at` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
	PRIMARY KEY (`id`)
);

CREATE TABLE `blsales`.`coefficient_r` (
	`id` INT NOT NULL AUTO_INCREMENT,
	`name` VARCHAR(6) NOT NULL DEFAULT '',
	`value` DOUBLE NOT NULL DEFAULT 0,
	PRIMARY KEY(`id`)
);

CREATE TABLE `blsales`.`coefficient_a` (
	`id` INT NOT NULL AUTO_INCREMENT,
	`name` VARCHAR(6) NOT NULL DEFAULT '',
	`value` DOUBLE NOT NULL DEFAULT 0,
	PRIMARY KEY(`id`)
);

CREATE TABLE `blsales`.`coefficient_b` (
	`id` INT NOT NULL AUTO_INCREMENT,
	`name` VARCHAR(6) NOT NULL DEFAULT '',
	`value` DOUBLE NOT NULL DEFAULT 0,
	PRIMARY KEY(`id`)
);

CREATE TABLE `blsales`.`coefficient_c` (
	`id` INT NOT NULL AUTO_INCREMENT,
	`name` VARCHAR(6) NOT NULL DEFAULT '',
	`value` DOUBLE NOT NULL DEFAULT 0,
	PRIMARY KEY(`id`)
);

ALTER TABLE `blsales`.`room` ADD CONSTRAINT `room_fk0` FOREIGN KEY (`room_type`) REFERENCES `room_types`(`id`);
ALTER TABLE `blsales`.`room` ADD CONSTRAINT `room_fk1` FOREIGN KEY (`group_id`) REFERENCES `room_group`(`id`);
ALTER TABLE `blsales`.`room_metrics` ADD CONSTRAINT `room_metrics_fk0` FOREIGN KEY (`room_type_id`) REFERENCES `room_types`(`id`);


-- +migrate Down
ALTER TABLE `blsales`.`room` DROP FOREIGN KEY `room_fk0`;
ALTER TABLE `blsales`.`room` DROP FOREIGN KEY `room_fk1`;
ALTER TABLE `blsales`.`room_metrics` DROP FOREIGN KEY `room_metrics_fk0`;

DROP TABLE IF EXISTS `blsales`.`users`;
DROP TABLE IF EXISTS `blsales`.`room`;
DROP TABLE IF EXISTS `blsales`.`room_types`;
DROP TABLE IF EXISTS `blsales`.`room_metrics`;
DROP TABLE IF EXISTS `blsales`.`room_group`;
DROP TABLE IF EXISTS `blsales`.`coefficient_r`;
DROP TABLE IF EXISTS `blsales`.`coefficient_a`;
DROP TABLE IF EXISTS `blsales`.`coefficient_b`;
DROP TABLE IF EXISTS `blsales`.`coefficient_c`;
