--
-- Installation migration of base data
--

-- +migrate Up
INSERT INTO `blsales`.`users` VALUES (0, 'admin@brainlit.se', SHA1('Flood15Voice!'), 0, NOW(), NOW(), NULL);
INSERT INTO `blsales`.`users` VALUES (0, 'lokisinclair', SHA1('Hunt32Items!'), 0, NOW(), NOW(), NULL);

INSERT INTO `blsales`.`room_types` VALUES (0, 'Bedroom', NOW(), NOW(), NULL);
INSERT INTO `blsales`.`room_types` VALUES (0, 'Ward', NOW(), NOW(), NULL);
INSERT INTO `blsales`.`room_types` VALUES (0, 'Living Room', NOW(), NOW(), NULL);
INSERT INTO `blsales`.`room_types` VALUES (0, 'Dining Room', NOW(), NOW(), NULL);
INSERT INTO `blsales`.`room_types` VALUES (0, 'Dining Hall', NOW(), NOW(), NULL);
INSERT INTO `blsales`.`room_types` VALUES (0, 'Corridor', NOW(), NOW(), NULL);
INSERT INTO `blsales`.`room_types` VALUES (0, 'Work Space (Offices)', NOW(), NOW(), NULL);
INSERT INTO `blsales`.`room_types` VALUES (0, 'Bathroom', NOW(), NOW(), NULL);

INSERT INTO `blsales`.`coefficient_r` VALUES (0, 'C1', -0.0343);
INSERT INTO `blsales`.`coefficient_r` VALUES (0, 'C2', 0.4062);
INSERT INTO `blsales`.`coefficient_r` VALUES (0, 'C3', -0.0320);
INSERT INTO `blsales`.`coefficient_r` VALUES (0, 'C4', -0.3396);
INSERT INTO `blsales`.`coefficient_r` VALUES (0, 'C5', -1.0745);
INSERT INTO `blsales`.`coefficient_r` VALUES (0, 'C6', 0.5865);
INSERT INTO `blsales`.`coefficient_r` VALUES (0, 'C7', 1.2763);
INSERT INTO `blsales`.`coefficient_r` VALUES (0, 'C8', 0.0186);
INSERT INTO `blsales`.`coefficient_r` VALUES (0, 'C9', 0.2911);
INSERT INTO `blsales`.`coefficient_r` VALUES (0, 'C10', 0.3608);
INSERT INTO `blsales`.`coefficient_r` VALUES (0, 'C11', -0.4834);
INSERT INTO `blsales`.`coefficient_r` VALUES (0, 'C12', -0.8229);
INSERT INTO `blsales`.`coefficient_r` VALUES (0, 'C13', 0.6794);
INSERT INTO `blsales`.`coefficient_r` VALUES (0, 'C14', 0.3079);
INSERT INTO `blsales`.`coefficient_r` VALUES (0, 'C15', -0.0355);
INSERT INTO `blsales`.`coefficient_r` VALUES (0, 'C16', -0.1511);
INSERT INTO `blsales`.`coefficient_r` VALUES (0, 'C17', 0.0075);
INSERT INTO `blsales`.`coefficient_r` VALUES (0, 'C18', 0.1806);
INSERT INTO `blsales`.`coefficient_r` VALUES (0, 'C19', -0.5365);
INSERT INTO `blsales`.`coefficient_r` VALUES (0, 'C20', 0.3961);

-- INSERT INTO `blsales`.`coefficient_r` (c1, c2, c3, c4, c5, c6, c7, c8, c9, c10, c11, c12, c13, c14, c15, c16, c17, c18, c19, c20) VALUES (-0.0343,
-- 0.4062, -0.0320, -0.3396, -1.0745, 0.5865, 1.2763, 0.0186, 0.2911, 0.3608,-0.4834, -0.8229, 0.6794, 0.3079, -0.0355, -0.1511,
-- 0.0075, 0.1806, -0.5365, 0.3961);

INSERT INTO `blsales`.`coefficient_a` VALUES (0, 'CA1', 0.006252218);
INSERT INTO `blsales`.`coefficient_a` VALUES (0, 'CA2', 0.013434248);
INSERT INTO `blsales`.`coefficient_a` VALUES (0, 'CA3', -0.000492717);
INSERT INTO `blsales`.`coefficient_a` VALUES (0, 'CA4', -0.008188143);
INSERT INTO `blsales`.`coefficient_a` VALUES (0, 'CA5', 1.83e-04);
INSERT INTO `blsales`.`coefficient_a` VALUES (0, 'CA6', 2.00E-04);
INSERT INTO `blsales`.`coefficient_a` VALUES (0, 'CA7', -0.607366038);
INSERT INTO `blsales`.`coefficient_a` VALUES (0, 'CA8', 1.27e-05);
INSERT INTO `blsales`.`coefficient_a` VALUES (0, 'CA9', -1.44e-04);
INSERT INTO `blsales`.`coefficient_a` VALUES (0, 'CA10', -5.56e-05);

-- INSERT INTO `blsales`.`coefficient_a` (ca1, ca2, ca3, ca4, ca5, ca6, ca7, ca8, ca9, ca10) VALUES (0.006252218, 0.013434248, -0.000492717, 
-- -0.008188143, 1.83e-04, 2.00E-04, -0.607366038, 1.27e-05, -1.44e-04, -5.56e-05);

INSERT INTO `blsales`.`coefficient_b` VALUES (0, 'CB1', -0.002493355);
INSERT INTO `blsales`.`coefficient_b` VALUES (0, 'CB2', -1.96e-03);
INSERT INTO `blsales`.`coefficient_b` VALUES (0, 'CB3', 1.30e-04);
INSERT INTO `blsales`.`coefficient_b` VALUES (0, 'CB4', 0.000850011);
INSERT INTO `blsales`.`coefficient_b` VALUES (0, 'CB5', -2.84e-05);
INSERT INTO `blsales`.`coefficient_b` VALUES (0, 'CB6', -1.69e-05);
INSERT INTO `blsales`.`coefficient_b` VALUES (0, 'CB7', -0.435119321);
INSERT INTO `blsales`.`coefficient_b` VALUES (0, 'CB8', -5.02e-05);
INSERT INTO `blsales`.`coefficient_b` VALUES (0, 'CB9', 9.36e-06);
INSERT INTO `blsales`.`coefficient_b` VALUES (0, 'CB10', 1.47e-05);

-- INSERT INTO `blsales`.`coefficient_b` (cb1, cb2, cb3, cb4, cb5, cb6, cb7, cb8, cb9, cb10) VALUES (-0.002493355, -1.96e-03, 1.30e-04, 0.000850011,
-- -2.84e-05, -1.69e-05, -0.435119321, -5.02e-05, 9.36e-06 ,1.47e-05);

INSERT INTO `blsales`.`coefficient_c` VALUES (0, 'CC1', -0.007559655);
INSERT INTO `blsales`.`coefficient_c` VALUES (0, 'CC2', -0.015127998);
INSERT INTO `blsales`.`coefficient_c` VALUES (0, 'CC3', 0.000508643);
INSERT INTO `blsales`.`coefficient_c` VALUES (0, 'CC4', 0.005710512);
INSERT INTO `blsales`.`coefficient_c` VALUES (0, 'CC5', -1.54e-04);
INSERT INTO `blsales`.`coefficient_c` VALUES (0, 'CC6', -1.68e-04);
INSERT INTO `blsales`.`coefficient_c` VALUES (0, 'CC7', 1.084072531);
INSERT INTO `blsales`.`coefficient_c` VALUES (0, 'CC8', 2.52e-05);
INSERT INTO `blsales`.`coefficient_c` VALUES (0, 'CC9', 1.51e-04);
INSERT INTO `blsales`.`coefficient_c` VALUES (0, 'CC10', 5.88e-05);


-- INSERT INTO `blsales`.`coefficient_c` (cc1, cc2, cc3, cc4, cc5, cc6, cc7, cc8, cc9, cc10) VALUES (-0.007559655, -0.015127998, 0.000508643,
-- 0.005710512, -1.54e-04, -1.68e-04, 1.084072531, 2.52e-05, 1.51e-04, 5.88e-05);

-- +migrate Down
DELETE FROM `blsales`.`users` WHERE username='admin@brainlit.se' LIMIT 1;
DELETE FROM `blsales`.`users` WHERE username='lokisinclair' LIMIT 1;

DELETE FROM `blsales`.`room_types` WHERE `name`='Bedroom' LIMIT 1;
DELETE FROM `blsales`.`room_types` WHERE `name`='Ward' LIMIT 1;
DELETE FROM `blsales`.`room_types` WHERE `name`='Living Room' LIMIT 1;
DELETE FROM `blsales`.`room_types` WHERE `name`='Dining Room' LIMIT 1;
DELETE FROM `blsales`.`room_types` WHERE `name`='Dining Hall' LIMIT 1;
DELETE FROM `blsales`.`room_types` WHERE `name`='Corridor' LIMIT 1;
DELETE FROM `blsales`.`room_types` WHERE `name`='Work Space (Offices)' LIMIT 1;
DELETE FROM `blsales`.`room_types` WHERE `name`='Bathroom' LIMIT 1;

DELETE FROM `blsales`.`coefficient_r`;
DELETE FROM `blsales`.`coefficient_a`;
DELETE FROM `blsales`.`coefficient_b`;
DELETE FROM `blsales`.`coefficient_c`;