--
-- Add formula's for parts R, A, B and C to database for editing
--

-- +migrate Up
CREATE TABLE `blsales`.`formulas` (
    `id` INT NOT NULL AUTO_INCREMENT,
    `formula` TEXT NOT NULL,
    `formula_calculation_part` VARCHAR(1) NOT NULL UNIQUE,
    PRIMARY KEY(`id`)
);

INSERT INTO `blsales`.`formulas` VALUES (0, 'C1*B + C2*(B*B) + C3*G + C4*G*B + C5*G*(B*B) + C6*G*(G*G) + C7*(G*G)*B + C8*R + C9*R*B + C10*R*(B*B) + C11*R*G + C12*R*G*B + C13*R*(G*G) + C14*(R*R) + C15*(R*R)*B + C16*(R*R)*G + C17*1 + C18*(R*R*R) + C19*(G*G*G) + C20*(B*B*B)', 'R');
INSERT INTO `blsales`.`formulas` VALUES (0, 'CA1*C + CA2*F + CA3*F*C + CA4*W + CA5*W*C + CA6*W*F + CA7 + CA8*(W*W) + CA9*(F*F) + CA10*(C*C)', 'A');
INSERT INTO `blsales`.`formulas` VALUES (0, 'CB1*C + CB2*F + CB3*F*C + CB4*W + CB5*W*C + CB6*W*F + CB7 + CB8*(W*W) + CB9*(F*F) + CB10*(C*C)', 'B');
INSERT INTO `blsales`.`formulas` VALUES (0, 'CC1*C + CC2*F + CC3*F*C + CC4*W + CC5*W*C + CC6*W*F + CC7 + CC8*(W*W) + CC9*(F*F) + CC10*(C*C)', 'C');

-- +migrate Down
DROP TABLE IF EXISTS `blsales`.`formulas`;