--
--
--

-- +migrate Up
CREATE TABLE `blsales`.`project` (
	`id` INT NOT NULL AUTO_INCREMENT,
	`name` varchar(256) NOT NULL,
	`location` varchar(256) NOT NULL,
	`timezone` varchar(256) NOT NULL,
	`customer_id` INT NOT NULL,
	`coords` varchar(64) NOT NULL,
	`facility_type` INT NOT NULL,
	`verified_by_customer` INT NOT NULL,
	`verified_by_bl` INT NOT NULL,
    `user_id` INT NOT NULL,
	PRIMARY KEY (`id`)
);

CREATE TABLE `blsales`.`customer` (
	`id` INT NOT NULL AUTO_INCREMENT,
	`name` varchar(64) NOT NULL,
	`contact_email` varchar(128) NOT NULL,
	`sales_contact_id` INT NOT NULL,
	PRIMARY KEY (`id`)
);

-- +migrate Down
DROP TABLE IF EXISTS `blsales`.`project`;
DROP TABLE IF EXISTS `blsales`.`customer`;