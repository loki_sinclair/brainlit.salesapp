--
--
--

-- +migrate Up
CREATE TABLE `blsales`.`project_block` (
	`id` INT NOT NULL AUTO_INCREMENT,
	`project_id` INT NOT NULL DEFAULT 0,
	`name` varchar(64) NOT NULL DEFAULT '',
	`created_at` DATETIME NOT NULL DEFAULT '0000-00-00 00:00:00',
	`updated_at` DATETIME NOT NULL DEFAULT '0000-00-00 00:00:00',
	`deleted_at` DATETIME NOT NULL DEFAULT '0000-00-00 00:00:00',
	PRIMARY KEY (`id`, `project_id`)
);

-- +migrate Down
DROP TABLE IF EXISTS `blsales`.`project_block`;