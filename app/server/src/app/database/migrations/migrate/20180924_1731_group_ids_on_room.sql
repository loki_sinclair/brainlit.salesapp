--
--
--

-- +migrate Up
ALTER TABLE `blsales`.`room` ADD COLUMN `block_id` INT NOT NULL DEFAULT 0;
ALTER TABLE `blsales`.`room` ADD COLUMN `project_id` INT NOT NULL DEFAULT 0;

-- +migrate Down
ALTER TABLE `blsales`.`room` DROP COLUMN `block_id`;
ALTER TABLE `blsales`.`room` DROP COLUMN `project_id`;