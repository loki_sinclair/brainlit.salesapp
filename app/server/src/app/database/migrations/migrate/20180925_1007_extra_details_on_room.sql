--
--
--

-- +migrate Up
ALTER TABLE `blsales`.`room` ADD COLUMN `room_length` DOUBLE NOT NULL DEFAULT 0;
ALTER TABLE `blsales`.`room` ADD COLUMN `room_width` DOUBLE NOT NULL DEFAULT 0;
ALTER TABLE `blsales`.`room` ADD COLUMN `ceiling_height` DOUBLE NOT NULL DEFAULT 0;
ALTER TABLE `blsales`.`room` ADD COLUMN `ceiling_color_hex` VARCHAR(8) NOT NULL DEFAULT '';
ALTER TABLE `blsales`.`room` ADD COLUMN `wall_color_hex` VARCHAR(8) NOT NULL DEFAULT '';
ALTER TABLE `blsales`.`room` ADD COLUMN `floor_color_hex` VARCHAR(8) NOT NULL DEFAULT '';
ALTER TABLE `blsales`.`room` ADD COLUMN `ceiling_color_rgb` VARCHAR(12) NOT NULL DEFAULT '';
ALTER TABLE `blsales`.`room` ADD COLUMN `wall_color_rgb` VARCHAR(12) NOT NULL DEFAULT '';
ALTER TABLE `blsales`.`room` ADD COLUMN `floor_color_rgb` VARCHAR(12) NOT NULL DEFAULT '';

-- +migrate Down
ALTER TABLE `blsales`.`room` DROP COLUMN `room_length`;
ALTER TABLE `blsales`.`room` DROP COLUMN `room_width`;
ALTER TABLE `blsales`.`room` DROP COLUMN `ceiling_height`;
ALTER TABLE `blsales`.`room` DROP COLUMN `ceiling_color_hex`;
ALTER TABLE `blsales`.`room` DROP COLUMN `wall_color_hex`;
ALTER TABLE `blsales`.`room` DROP COLUMN `floor_color_hex`;
ALTER TABLE `blsales`.`room` DROP COLUMN `ceiling_color_rgb`;
ALTER TABLE `blsales`.`room` DROP COLUMN `wall_color_rgb`;
ALTER TABLE `blsales`.`room` DROP COLUMN `floor_color_rgb`;