--
--
--

-- +migrate Up
ALTER TABLE `blsales`.`room` ADD COLUMN `num_sensor` INT NOT NULL DEFAULT 0;
ALTER TABLE `blsales`.`room` ADD COLUMN `num_keypads` INT NOT NULL DEFAULT 0;
ALTER TABLE `blsales`.`room` ADD COLUMN `num_luminaires` INT NOT NULL DEFAULT 0;

-- +migrate Down
ALTER TABLE `blsales`.`room` DROP COLUMN `num_sensor`;
ALTER TABLE `blsales`.`room` DROP COLUMN `num_keypads`;
ALTER TABLE `blsales`.`room` DROP COLUMN `num_luminaires`;