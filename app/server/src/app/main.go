package main

import (
	"flag"
	"fmt"
	"log"
	"os"
	"sync"
	"time"

	"bitbucket.org/presis/brainlit.salesapp/app/server/src/app/common"
	"bitbucket.org/presis/brainlit.salesapp/app/server/src/app/middleware"
	"bitbucket.org/presis/brainlit.salesapp/app/server/src/app/routes"

	jwt "github.com/appleboy/gin-jwt"
	"github.com/gin-gonic/gin"
	"github.com/joho/godotenv"
)

var (
	identityKey = "id"
	mutex       = &sync.Mutex{}
)

type login struct {
	Username string `form:"username" json:"username" binding:"required"`
	Password string `form:"password" json:"password" binding:"required"`
}

func main() {
	err := godotenv.Load()
	if err != nil {
		log.Fatal(err)
		os.Exit(1)
	}

	//debugMode := flag.Bool("debug", false, "Whether or not to allow Debug mode (no auth)")
	migrateMode := flag.Bool("migrate", false, "Whether or not to only install/update db migrations")
	direction := flag.String("direction", "up", "Whether we're installing or rolling back the db migrations")
	flag.Parse()

	// When running in migration mode only, prep the db and stop
	if bool(*migrateMode) {
		runMigrationsOnly(*direction)
		return
	}

	router := gin.Default()
	authMiddleware := &jwt.GinJWTMiddleware{
		Realm:      "blsalesapp",
		Key:        []byte(os.Getenv("WEB_PORTAL_SECRET")),
		Timeout:    time.Hour,
		MaxRefresh: time.Hour,
		PayloadFunc: func(data interface{}) jwt.MapClaims {
			if v, ok := data.(*common.User); ok {
				return jwt.MapClaims{
					identityKey: v.Id,
				}
			}
			return jwt.MapClaims{}
		},
		Authenticator: func(c *gin.Context) (interface{}, error) {
			var loginVals login
			if err := c.BindJSON(&loginVals); err != nil {
				panic(jwt.ErrMissingLoginValues)
			}

			db := common.GetDBInstance()
			stmt, err := db.Connection.Prepare("SELECT * FROM users WHERE username=? AND password=SHA(?)")
			if err != nil {
				panic(err)
			}
			defer stmt.Close()

			rows, err := stmt.Query(loginVals.Username, loginVals.Password)
			fmt.Printf("%s / %s", loginVals.Username, loginVals.Password)
			if err != nil {
				panic(err)
			}
			defer rows.Close()
			var user common.User
			for rows.Next() {
				err := rows.Scan(&user.Id, &user.Username, &user.Password,
					&user.Perms, &user.CreatedAt, &user.UpdatedAt, &user.DeletedAt)
				if err = rows.Err(); err != nil {
					panic(err)
				}
			}

			if user.Id > 0 {
				c.Writer.Header().Set("Content-Type", "application/json")
				c.Writer.Header().Set("Access-Control-Allow-Origin", "*")
				c.Writer.Header().Set("Access-Control-Allow-Headers",
					"Content-Type, Content-Length, Accept-Encoding, X-CSRF-Token, Authorization")

				return &common.User{
					Id:        user.Id,
					Username:  user.Username,
					Perms:     user.Perms,
					CreatedAt: user.CreatedAt,
					UpdatedAt: user.UpdatedAt,
					DeletedAt: user.DeletedAt,
				}, nil
			}

			return nil, jwt.ErrFailedAuthentication
		},
		Authorizator: func(data interface{}, c *gin.Context) bool {
			if _, ok := data.(float64); ok {
				return true
			}
			return false
		},
		Unauthorized: func(c *gin.Context, code int, message string) {
			c.Writer.Header().Set("Content-Type", "application/json")
			c.Writer.Header().Set("Access-Control-Allow-Origin", "*")
			c.Writer.Header().Set("Access-Control-Allow-Headers",
				"Content-Type, Content-Length, Accept-Encoding, X-CSRF-Token, Authorization")
			c.JSON(code, gin.H{
				"code":    code,
				"message": message,
			})
		},
		// TokenLookup is a string in the form of "<source>:<name>" that is used
		// to extract token from the request.
		// Optional. Default value "header:Authorization".
		// Possible values:
		// - "header:<name>"
		// - "query:<name>"
		// - "cookie:<name>"
		TokenLookup: "header: Authorization, query: token, cookie: jwt",
		// TokenLookup: "query:token",
		// TokenLookup: "cookie:token",

		// TokenHeadName is a string in the header. Default value is "Bearer"
		TokenHeadName: "Bearer",
		// TimeFunc provides the current time. You can override it to use another time
		// value. This is useful for testing or if your server uses different time zones
		TimeFunc: time.Now,
	}

	router.POST("/login", authMiddleware.LoginHandler)
	// Routes protected by auth
	auth := router.Group("")
	auth.Use(authMiddleware.MiddlewareFunc())
	{
		auth.GET("/projects", routes.GetProjectsHandler)
		auth.GET("/roomtypes", routes.GetRoomTypesHandler)
		auth.GET("/projectblocks/:id", routes.GetProjectBlocksHandler)
		auth.GET("/projectblocks/:id/rooms", routes.GetProjectBlockRoomsHandler)
		auth.GET("/projectgroups/:id", routes.GetGroupsHandler)

		auth.GET("/projectblocks/:id/count", routes.GetBlockCountHandler)

		auth.POST("/projects/create", routes.CreateProjectsHandler)
		auth.POST("/projects/decline/:id", routes.DeclineProjectsHandler)
		auth.POST("/projects/approve/:id", routes.ApproveProjectsHandler)
		auth.POST("/projectblocks/create", routes.CreateBlocksHandler)
		auth.POST("/room/create", routes.CreateRoomsHandler)
		auth.POST("/room", routes.SaveRoomHandler)
		auth.POST("/projectgroups/create", routes.CreateGroupsHandler)

		auth.DELETE("/projects/:id", routes.DeleteProjectsHandler)
		auth.DELETE("/projectblocks/:id", routes.DeleteBlocksHandler)
	}

	router.POST("/calculate_luminaires", routes.CalculateLuminairesHandler)
	// CORS
	router.Use(middleware.CorsHandlerMiddleware())
	router.Run(":" + os.Getenv("WEB_PORTAL_PORT"))
}

func runMigrationsOnly(dir string) {
	var db common.Database
	if string(dir) == "up" {
		_, err := db.MigrateAll()
		if err != nil {
			panic(err)
		}
	} else {
		_, err := db.MigrateDown()
		if err != nil {
			panic(err)
		}
	}
}
