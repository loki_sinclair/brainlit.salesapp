package routes

import (
	"net/http"

	"bitbucket.org/presis/brainlit.salesapp/app/server/src/app/common"
	"github.com/gin-gonic/gin"
)

type blockParams struct {
	ProjectId int    `form:"projectId" json:"projectId" binding:"required"`
	Name      string `form:"blockName" json:"blockName" binding:"required"`
}

// GetProjectBlocksHandler Returns a list of blocks linked to specific project id
func GetProjectBlocksHandler(c *gin.Context) {
	db := common.GetDBInstance()
	stmt, err := db.Connection.Prepare("SELECT * FROM project_block WHERE project_id = ?")
	if err != nil {
		panic(err)
	}
	defer stmt.Close()

	rows, err := stmt.Query(c.Param("id"))
	if err != nil {
		panic(err)
	}
	defer rows.Close()

	var blocks []common.ProjectBlock
	for rows.Next() {
		var b common.ProjectBlock
		err := rows.Scan(&b.ID, &b.ProjectID, &b.Name, &b.CreatedAt, &b.UpdatedAt, &b.DeletedAt)
		if err = rows.Err(); err != nil {
			panic(err)
		}

		blocks = append(blocks, b)
	}

	c.Writer.Header().Set("Content-Type", "application/json")
	c.Writer.Header().Set("Access-Control-Allow-Origin", "*")
	c.Writer.Header().Set("Access-Control-Allow-Headers",
		"Content-Type, Content-Length, Accept-Encoding, X-CSRF-Token, Authorization")
	c.JSON(http.StatusOK, gin.H{
		"message": "OK",
		"data":    blocks,
	})
}

// GetProjectBlockRoomsHandler Returns a list of rooms assigned to a project block
func GetProjectBlockRoomsHandler(c *gin.Context) {
	db := common.GetDBInstance()
	stmt, err := db.Connection.Prepare(`SELECT * FROM room WHERE block_id = ? ORDER BY id ASC`)
	if err != nil {
		panic(err)
	}
	defer stmt.Close()

	rows, err := stmt.Query(c.Param("id"))
	if err != nil {
		panic(err)
	}
	defer rows.Close()

	var rooms []common.Room
	for rows.Next() {
		var r common.Room
		err := rows.Scan(&r.ID, &r.Name, &r.RoomType, &r.CreatedAt, &r.UpdatedAt, &r.DeletedAt,
			&r.GroupId, &r.BlockId, &r.ProjectId, &r.RoomLength, &r.RoomWidth, &r.CeilingHeight,
			&r.CeilingColorHex, &r.WallColorHex, &r.FloorColorHex, &r.CeilingColorRgb, &r.WallColorRgb,
			&r.FloorColorRgb, &r.NumSensor, &r.NumKeypads, &r.NumLuminaires)
		if err != nil {
			panic(err)
		}
		if err = rows.Err(); err != nil {
			panic(err)
		}
		rooms = append(rooms, r)
	}

	c.Writer.Header().Set("Content-Type", "application/json")
	c.Writer.Header().Set("Access-Control-Allow-Origin", "*")
	c.Writer.Header().Set("Access-Control-Allow-Headers",
		"Content-Type, Content-Length, Accept-Encoding, X-CSRF-Token, Authorization")
	c.JSON(http.StatusOK, gin.H{
		"message": "OK",
		"data":    rooms,
	})
}

// GetBlockCountHandler
func GetBlockCountHandler(c *gin.Context) {
	db := common.GetDBInstance()
	stmt, err := db.Connection.Prepare("SELECT COUNT(*) AS `count` FROM project_block WHERE project_id = ?")
	if err != nil {
		panic(err)
	}
	defer stmt.Close()
	rows, err := stmt.Query(c.Param("id"))
	if err != nil {
		panic(err)
	}
	defer rows.Close()

	var count int
	for rows.Next() {
		err := rows.Scan(&count)
		if err = rows.Err(); err != nil {
			panic(err)
		}
	}

	c.Writer.Header().Set("Content-Type", "application/json")
	c.Writer.Header().Set("Access-Control-Allow-Origin", "*")
	c.Writer.Header().Set("Access-Control-Allow-Headers",
		"Content-Type, Content-Length, Accept-Encoding, X-CSRF-Token, Authorization")
	c.JSON(http.StatusOK, gin.H{
		"message": "OK",
		"data":    count,
	})
}

// DeleteBlocksHandler
func DeleteBlocksHandler(c *gin.Context) {
	db := common.GetDBInstance()

	stmt, err := db.Connection.Prepare("DELETE FROM project_block WHERE id = ?")
	if err != nil {
		panic(err)
	}
	defer stmt.Close()
	_, err = stmt.Query(c.Param("id"))
	if err != nil {
		panic(err)
	}

	c.Writer.Header().Set("Content-Type", "application/json")
	c.Writer.Header().Set("Access-Control-Allow-Origin", "*")
	c.Writer.Header().Set("Access-Control-Allow-Headers",
		"Content-Type, Content-Length, Accept-Encoding, X-CSRF-Token, Authorization")
	c.JSON(http.StatusOK, gin.H{
		"message": "block_deleted",
	})
}

// CreateBlocksHandler
func CreateBlocksHandler(c *gin.Context) {
	// claims := jwt.ExtractClaims(c)
	db := common.GetDBInstance()

	var block blockParams
	if err := c.BindJSON(&block); err != nil {
		panic("Missing required block parameters")
	}

	stmt, err := db.Connection.Prepare("INSERT INTO project_block VALUES (0, ?, ?, NOW(), NOW(), '0000-00-00 00:00:00')")
	if err != nil {
		panic(err)
	}
	defer stmt.Close()

	_, err = stmt.Query(&block.ProjectId, &block.Name)
	if err != nil {
		panic(err)
	}

	c.Writer.Header().Set("Content-Type", "application/json")
	c.Writer.Header().Set("Access-Control-Allow-Origin", "*")
	c.Writer.Header().Set("Access-Control-Allow-Headers",
		"Content-Type, Content-Length, Accept-Encoding, X-CSRF-Token, Authorization")
	c.JSON(http.StatusOK, gin.H{
		"message": "block_created",
	})
}
