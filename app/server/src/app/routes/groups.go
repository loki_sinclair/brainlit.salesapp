package routes

import (
	"fmt"
	"net/http"

	"bitbucket.org/presis/brainlit.salesapp/app/server/src/app/common"
	"github.com/gin-gonic/gin"
)

type groupParams struct {
	ProjectId int    `form:"projectId" json:"projectId" binding:"required"`
	GroupName string `form:"groupName" json:"groupName" binding:"required"`
}

// GetGroupsHandler Returns a list of groups assigned to a project id
func GetGroupsHandler(c *gin.Context) {
	db := common.GetDBInstance()
	stmt, err := db.Connection.Prepare("SELECT * FROM project_group WHERE project_id = ?")
	if err != nil {
		panic(err)
	}
	defer stmt.Close()

	rows, err := stmt.Query(c.Param("id"))
	if err != nil {
		panic(err)
	}
	defer rows.Close()

	var groups []common.ProjectGroup
	for rows.Next() {
		var g common.ProjectGroup
		err := rows.Scan(&g.ID, &g.ProjectID, &g.Name, &g.CreatedAt, &g.UpdatedAt, &g.DeletedAt)
		if err = rows.Err(); err != nil {
			panic(err)
		}

		groups = append(groups, g)
	}

	c.Writer.Header().Set("Content-Type", "application/json")
	c.Writer.Header().Set("Access-Control-Allow-Origin", "*")
	c.Writer.Header().Set("Access-Control-Allow-Headers",
		"Content-Type, Content-Length, Accept-Encoding, X-CSRF-Token, Authorization")
	c.JSON(http.StatusOK, gin.H{
		"message": "OK",
		"data":    groups,
	})
}

// CreateGroupsHandler Creates a group linked to a project id
func CreateGroupsHandler(c *gin.Context) {
	db := common.GetDBInstance()

	var gParams groupParams
	if err := c.BindJSON(&gParams); err != nil {
		panic("Missing required group paramters")
	}

	stmt, err := db.Connection.Prepare("INSERT INTO project_group VALUES (0, ?, ?, NOW(), NOW(), '0000-00-00 00:00:00')")
	if err != nil {
		panic(err)
	}
	defer stmt.Close()

	fmt.Printf("Got %v\n", gParams)
	_, err = stmt.Query(&gParams.ProjectId, &gParams.GroupName)
	if err != nil {
		panic(err)
	}

	c.Writer.Header().Set("Content-Type", "application/json")
	c.Writer.Header().Set("Access-Control-Allow-Origin", "*")
	c.Writer.Header().Set("Access-Control-Allow-Headers",
		"Content-Type, Content-Length, Accept-Encoding, X-CSRF-Token, Authorization")
	c.JSON(http.StatusOK, gin.H{
		"message": "group_created",
	})
}
