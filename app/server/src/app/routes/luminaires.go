package routes

import (
	"fmt"
	"net/http"

	"bitbucket.org/presis/brainlit.salesapp/app/server/src/app/common"
	"bitbucket.org/presis/brainlit.salesapp/app/server/src/app/tools"
	"github.com/gin-gonic/gin"
)

type luminaireParams struct {
	RoomLength        float64 `form:"roomLength" json:"roomLength" binding:"required"`
	RoomWidth         float64 `form:"roomWidth" json:"roomWidth" binding:"required"`
	CeilingHeight     float64 `form:"ceilingHeight" json:"ceilingHeight" binding:"required"`
	RequiredLuminance int     `form:"requiredLux" json:"requiredLux" binding:"required"`
	CeilingColor      string  `form:"ceilingColor" json:"ceilingColor" binding:"required"`
	WallColor         string  `form:"wallColor" json:"wallColor" binding:"required"`
	FloorColor        string  `form:"floowColor" json:"floorColor" binding:"required"`
}

// CalculateLuminairesHandler Returns estimated number of luminaires
// required to properly illuminate a room space, defined by parameters
// passed
func CalculateLuminairesHandler(c *gin.Context) {
	var params common.CalculationParameters
	var lc = new(tools.LuminaireCalculator)

	var lumParams luminaireParams
	if err := c.BindJSON(&lumParams); err != nil {
		fmt.Printf("Missing params - got %v", lumParams)
		panic("Missing required calculation parameters")
	}

	params.CoefficientsR, params.CoefficientsA, params.CoefficientsB, params.CoefficientsC = LoadCoefficients()
	params.CeilingColor = lumParams.CeilingColor
	params.WallColor = lumParams.WallColor
	params.FloorColor = lumParams.FloorColor
	params.RoomLength = float32(lumParams.RoomLength)
	params.RoomWidth = float32(lumParams.RoomWidth)
	params.CeilingHeight = float32(lumParams.CeilingHeight)
	params.RequiredLuminance = lumParams.RequiredLuminance

	lc.Parameters = &params
	lc.Parameters.ConvertCeilingRGB(params.CeilingColor)
	lc.Parameters.ConvertWallRGB(params.WallColor)
	lc.Parameters.ConvertFloorRGB(params.FloorColor)
	lc.Formulas, _ = LoadFormulas()

	ceilingReflectivity := float64(lc.CalculateReflectivity(lc.Parameters, lc.Formulas[0], params.CeilingColorRGB[0],
		params.CeilingColorRGB[1], params.CeilingColorRGB[2]))
	wallReflectivity := float64(lc.CalculateReflectivity(lc.Parameters, lc.Formulas[0], params.WallColorRGB[0],
		params.WallColorRGB[1], params.WallColorRGB[2]))
	floorReflectivity := float64(lc.CalculateReflectivity(lc.Parameters, lc.Formulas[0], params.FloorColorRGB[0],
		params.FloorColorRGB[1], params.FloorColorRGB[2]))

	factorA := lc.CalculateA(lc.Parameters, lc.Formulas[1], ceilingReflectivity, wallReflectivity, floorReflectivity)
	factorB := lc.CalculateB(lc.Parameters, lc.Formulas[2], ceilingReflectivity, wallReflectivity, floorReflectivity)
	factorC := lc.CalculateC(lc.Parameters, lc.Formulas[3], ceilingReflectivity, wallReflectivity, floorReflectivity)

	factorK := lc.CalculateRoomIndex(params.RoomWidth, params.RoomLength,
		params.CeilingHeight)
	utilizationFactor := lc.CalculateUF(factorK, factorA, factorB, factorC)
	numLuminaires := lc.CalculateLuminaires(float64(params.RequiredLuminance),
		float64((params.RoomWidth * params.RoomLength)), 3287, utilizationFactor)

	c.Writer.Header().Set("Content-Type", "application/json")
	c.Writer.Header().Set("Access-Control-Allow-Origin", "*")
	c.Writer.Header().Set("Access-Control-Allow-Headers",
		"Content-Type, Content-Length, Accept-Encoding, X-CSRF-Token, Authorization")

	c.JSON(http.StatusOK, gin.H{
		"message": "OK",
		"data": map[string]float64{
			"ceilingReflectivity": ceilingReflectivity,
			"wallReflectivity":    wallReflectivity,
			"floorReflectivity":   floorReflectivity,
			"numLuminaires":       numLuminaires,
		},
	})
}

// LoadFormulas Loads in formula data from db tables, ready for calculating
func LoadFormulas() ([]common.Formula, error) {
	var formulas []common.Formula
	db := common.GetDBInstance()
	stmt, err := db.Connection.Prepare("SELECT * FROM formulas ORDER BY id ASC")
	if err != nil {
		panic(err)
	}
	defer stmt.Close()

	rows, err := stmt.Query()
	if err != nil {
		panic(err)
	}
	defer rows.Close()

	var formula common.Formula
	for rows.Next() {
		err := rows.Scan(&formula.Id, &formula.Formula, &formula.FormualCalculationPart)
		if err = rows.Err(); err != nil {
			panic(err)
		}
		formulas = append(formulas, formula)
	}

	return formulas, nil
}

// LoadCoefficients Load and return all Coefficient values from database tables
func LoadCoefficients() ([]common.CoefficientR, []common.CoefficientA,
	[]common.CoefficientB, []common.CoefficientC) {
	// Read Coefficients R
	db := common.GetDBInstance()
	stmt, err := db.Connection.Prepare("SELECT * FROM coefficient_r")
	if err != nil {
		panic(err)
	}
	defer stmt.Close()

	rows, err := stmt.Query()
	if err != nil {
		panic(err)
	}
	defer rows.Close()

	var coefficientRs []common.CoefficientR
	for rows.Next() {
		var cr common.CoefficientR
		err := rows.Scan(&cr.Id, &cr.Name, &cr.Value)
		if err = rows.Err(); err != nil {
			panic(err)
		}

		coefficientRs = append(coefficientRs, cr)
	}

	// Read Coeffficients A
	stmt, err = db.Connection.Prepare("SELECT * FROM coefficient_a")
	if err != nil {
		panic(err)
	}
	defer stmt.Close()

	rows, err = stmt.Query()
	if err != nil {
		panic(err)
	}
	defer rows.Close()

	var coefficientAs []common.CoefficientA
	for rows.Next() {
		var ca common.CoefficientA
		err := rows.Scan(&ca.Id, &ca.Name, &ca.Value)
		if err = rows.Err(); err != nil {
			panic(err)
		}

		coefficientAs = append(coefficientAs, ca)
	}

	// Read Coefficients B
	stmt, err = db.Connection.Prepare("SELECT * FROM coefficient_b")
	if err != nil {
		panic(err)
	}
	defer stmt.Close()

	rows, err = stmt.Query()
	if err != nil {
		panic(err)
	}
	defer rows.Close()

	var coefficientBs []common.CoefficientB
	for rows.Next() {
		var cb common.CoefficientB
		err := rows.Scan(&cb.Id, &cb.Name, &cb.Value)
		if err = rows.Err(); err != nil {
			panic(err)
		}

		coefficientBs = append(coefficientBs, cb)
	}

	// Read Coefficients C
	stmt, err = db.Connection.Prepare("SELECT * FROM coefficient_c")
	if err != nil {
		panic(err)
	}
	defer stmt.Close()

	rows, err = stmt.Query()
	if err != nil {
		panic(err)
	}
	defer rows.Close()

	var coefficientCs []common.CoefficientC
	for rows.Next() {
		var cc common.CoefficientC
		err := rows.Scan(&cc.Id, &cc.Name, &cc.Value)
		if err = rows.Err(); err != nil {
			panic(err)
		}

		coefficientCs = append(coefficientCs, cc)
	}

	return coefficientRs, coefficientAs, coefficientBs, coefficientCs
}
