package routes

import (
	"fmt"
	"net/http"

	"bitbucket.org/presis/brainlit.salesapp/app/server/src/app/common"
	jwt "github.com/appleboy/gin-jwt"
	"github.com/gin-gonic/gin"
)

type projectParams struct {
	Name     string `form:"name" json:"name" binding:"required"`
	Location string `form:"location" json:"location" binding:"required"`
	Timezone string `form:"timezone" json:"timezone" binding:"required"`
	Coords   string `form:"coords" json:"coords" binding:"required"`
}

// GetProjectsHandler Returns list of projects for the logged in user
// via usage of JWT token claims
func GetProjectsHandler(c *gin.Context) {
	claims := jwt.ExtractClaims(c)
	db := common.GetDBInstance()
	stmt, err := db.Connection.Prepare("SELECT * FROM project WHERE user_id = ?")
	if err != nil {
		panic(err)
	}
	defer stmt.Close()

	rows, err := stmt.Query(claims["id"].(float64))
	fmt.Printf("%f", claims["id"].(float64))
	if err != nil {
		panic(err)
	}
	defer rows.Close()

	var projects []common.Project
	for rows.Next() {
		var proj common.Project
		err := rows.Scan(&proj.ID, &proj.Name, &proj.Location, &proj.Timezone, &proj.CustomerID,
			&proj.Coords, &proj.FacilityType, &proj.VerifiedByCustomer, &proj.VerifiedByBL, &proj.UserID)
		if err = rows.Err(); err != nil {
			panic(err)
		}

		projects = append(projects, proj)
	}

	c.Writer.Header().Set("Content-Type", "application/json")
	c.Writer.Header().Set("Access-Control-Allow-Origin", "*")
	c.Writer.Header().Set("Access-Control-Allow-Headers",
		"Content-Type, Content-Length, Accept-Encoding, X-CSRF-Token, Authorization")
	c.JSON(http.StatusOK, gin.H{
		"message": "OK",
		"data": map[string][]common.Project{
			"projects": projects,
		},
	})
}

// CreateProjectsHandler Creates a project for the logged in user
// using JWT token claims
func CreateProjectsHandler(c *gin.Context) {
	claims := jwt.ExtractClaims(c)
	db := common.GetDBInstance()

	var projParams projectParams
	if err := c.BindJSON(&projParams); err != nil {
		panic("Missing required project parameters")
	}

	stmt, err := db.Connection.Prepare("INSERT INTO project VAlUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)")
	if err != nil {
		panic(err)
	}
	defer stmt.Close()

	_, err = stmt.Query(0, &projParams.Name, &projParams.Location,
		&projParams.Timezone, 0, &projParams.Coords, 0, 0, 0, claims["id"].(float64))
	if err != nil {
		panic(err)
	}

	c.Writer.Header().Set("Content-Type", "application/json")
	c.Writer.Header().Set("Access-Control-Allow-Origin", "*")
	c.Writer.Header().Set("Access-Control-Allow-Headers",
		"Content-Type, Content-Length, Accept-Encoding, X-CSRF-Token, Authorization")
	c.JSON(http.StatusOK, gin.H{
		"message": "project_created",
	})
}

// DeclineProjectsHandler Unapproves a project definition and removes
// it from queue of projects awaiting approval
func DeclineProjectsHandler(c *gin.Context) {
	db := common.GetDBInstance()
	stmt, err := db.Connection.Prepare("UPDATE project SET verified_by_customer=0, verified_by_bl=0 WHERE id = ?")
	if err != nil {
		panic(err)
	}
	defer stmt.Close()

	_, err = stmt.Query(c.Param("id"))
	if err != nil {
		panic(err)
	}

	c.Writer.Header().Set("Content-Type", "application/json")
	c.Writer.Header().Set("Access-Control-Allow-Origin", "*")
	c.Writer.Header().Set("Access-Control-Allow-Headers",
		"Content-Type, Content-Length, Accept-Encoding, X-CSRF-Token, Authorization")
	c.JSON(http.StatusOK, gin.H{
		"message": "OK",
		"data":    "project_declined",
	})
}

// ApproveProjectHandler Approves a project that is deemed complete
// and ready for overall project approval
func ApproveProjectsHandler(c *gin.Context) {
	db := common.GetDBInstance()
	stmt, err := db.Connection.Prepare("UPDATE project SET verified_by_customer=1 WHERE id = ?")
	if err != nil {
		panic(err)
	}
	defer stmt.Close()

	_, err = stmt.Query(c.Param("id"))
	if err != nil {
		panic(err)
	}

	c.Writer.Header().Set("Content-Type", "application/json")
	c.Writer.Header().Set("Access-Control-Allow-Origin", "*")
	c.Writer.Header().Set("Access-Control-Allow-Headers",
		"Content-Type, Content-Length, Accept-Encoding, X-CSRF-Token, Authorization")
	c.JSON(http.StatusOK, gin.H{
		"message": "OK",
		"data":    "project_approved",
	})
}

// DeleteProjectsHandler Removes a project from the system
func DeleteProjectsHandler(c *gin.Context) {
	db := common.GetDBInstance()

	stmt, err := db.Connection.Prepare("DELETE FROM project WHERE id = ?")
	if err != nil {
		panic(err)
	}
	defer stmt.Close()

	_, err = stmt.Query(c.Param("id"))
	if err != nil {
		panic(err)
	}

	c.Writer.Header().Set("Content-Type", "application/json")
	c.Writer.Header().Set("Access-Control-Allow-Origin", "*")
	c.Writer.Header().Set("Access-Control-Allow-Headers",
		"Content-Type, Content-Length, Accept-Encoding, X-CSRF-Token, Authorization")
	c.JSON(http.StatusOK, gin.H{
		"message": "project_deleted",
	})
}
