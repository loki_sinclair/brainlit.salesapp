package routes

import (
	"database/sql"
	"fmt"
	"net/http"

	"bitbucket.org/presis/brainlit.salesapp/app/server/src/app/common"
	"github.com/gin-gonic/gin"
)

func CreateRoomsHandler(c *gin.Context) {
	db := common.GetDBInstance()
	var roomParams common.Room
	err := c.BindJSON(&roomParams)
	if err != nil {
		panic(err)
	}

	stmt, err := db.Connection.Prepare(`INSERT INTO room VALUES (0, name=?, room_type=?, 
		created_at=NOW(), updated_at=NOW(), deleted_at='0000-00-00 00:00:00',
		group_id=?, block_id=?, project_id=?, room_length=?, room_width=?, ceiling_height=?,
		ceiling_color_hex=?, wall_color_hex=?, floor_color_hex=?, ceiling_color_rgb=?,
		wall_color_rgb=?, floor_color_rgb=?, num_sensor=?, num_keypads=?, num_luminaires=?)`)
	if err != nil {
		panic(err)
	}
	defer stmt.Close()

	rows, err := stmt.Query(&roomParams.Name, &roomParams.RoomType,
		&roomParams.GroupId, &roomParams.BlockId, &roomParams.ProjectId,
		&roomParams.RoomLength, &roomParams.RoomWidth, &roomParams.CeilingHeight,
		&roomParams.CeilingColorHex, &roomParams.WallColorHex, &roomParams.FloorColorHex,
		&roomParams.CeilingColorRgb, &roomParams.WallColorRgb, &roomParams.FloorColorRgb,
		&roomParams.NumSensor, &roomParams.NumKeypads, &roomParams.NumLuminaires)
	if err != nil {
		panic(err)
	}

	fmt.Printf("%v", rows)
}

// SaveRoomHandler Saves an instance of a room when updating
func SaveRoomHandler(c *gin.Context) {
	db := common.GetDBInstance()
	var stmt *sql.Stmt
	var err error

	var roomParams = common.GetRequestBodyAsMap(c)
	if roomParams["id"].(float64) < 0 {
		fmt.Println("Adding new room for ID -100")
		stmt, err := db.Connection.Prepare(`INSERT INTO room (id, name, room_type, created_at, 
			updated_at, deleted_at, group_id, block_id, project_id, room_length, room_width, ceiling_height,
			ceiling_color_hex, wall_color_hex, floor_color_hex, ceiling_color_rgb, wall_color_rgb,
			floor_color_rgb, num_sensor, num_keypads, num_luminaires) VALUES (0, ?, ?, NOW(), NOW(),
			'0000-00-00 00:00:00', ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)`)
		if err != nil {
			panic(err)
		}
		defer stmt.Close()

		res, err := stmt.Exec(roomParams["Name"].(string), roomParams["RoomType"].(float64),
			roomParams["GroupId"].(float64), roomParams["BlockId"].(float64), roomParams["ProjectId"].(float64),
			roomParams["RoomLength"].(float64), roomParams["RoomWidth"].(float64), roomParams["CeilingHeight"].(float64),
			roomParams["CeilingColorHex"].(string), roomParams["WallColorHex"].(string), roomParams["FloorColorHex"].(string),
			roomParams["CeilingColorRgb"].(string), roomParams["WallColorRgb"].(string), roomParams["FloorColorRgb"].(string),
			roomParams["NumSensor"].(float64), roomParams["NumKeypads"].(float64), roomParams["NumLuminaires"].(float64))
		if err != nil {
			panic(err)
		}

		id, err := res.LastInsertId()
		if err != nil {
			panic(err)
		}

		c.Writer.Header().Set("Content-Type", "application/json")
		c.Writer.Header().Set("Access-Control-Allow-Origin", "*")
		c.Writer.Header().Set("Access-Control-Allow-Headers",
			"Content-Type, Content-Length, Accept-Encoding, X-CSRF-Token, Authorization")
		c.JSON(http.StatusOK, gin.H{
			"message": "room_created",
			"id":      id,
		})
		return
	} else {
		fmt.Println("Updating existing room for ID")
		stmt, err = db.Connection.Prepare(`UPDATE room SET name=?, room_type=?, updated_at=NOW(),
			group_id=?, block_id=?, project_id=?, room_length=?, room_width=?, ceiling_height=?,
			ceiling_color_hex=?, wall_color_hex=?, floor_color_hex=?, ceiling_color_rgb=?,
			wall_color_rgb=?, floor_color_rgb=?, num_sensor=?, num_keypads=?, num_luminaires=? WHERE id = ?`)
		if err != nil {
			panic(err)
		}
		defer stmt.Close()

		_, err = stmt.Query(roomParams["Name"], roomParams["RoomType"],
			roomParams["GroupId"], roomParams["BlockId"], roomParams["ProjectId"],
			roomParams["RoomLength"], roomParams["RoomWidth"], roomParams["CeilingHeight"],
			roomParams["CeilingColorHex"], roomParams["WallColorHex"], roomParams["FloorColorHex"],
			roomParams["CeilingColorRgb"], roomParams["WallColorRgb"], roomParams["FloorColorRgb"],
			roomParams["NumSensor"], roomParams["NumKeypads"], roomParams["NumLuminaires"], roomParams["id"])
		if err != nil {
			panic(err)
		}

		c.Writer.Header().Set("Content-Type", "application/json")
		c.Writer.Header().Set("Access-Control-Allow-Origin", "*")
		c.Writer.Header().Set("Access-Control-Allow-Headers",
			"Content-Type, Content-Length, Accept-Encoding, X-CSRF-Token, Authorization")
		c.JSON(http.StatusOK, gin.H{
			"message": "room_updated",
		})
		return
	}
}
