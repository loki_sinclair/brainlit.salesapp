package routes

import (
	"net/http"

	"bitbucket.org/presis/brainlit.salesapp/app/server/src/app/common"
	"github.com/gin-gonic/gin"
)

// GetRoomTypesHandler Returns a list of pre-defined room types
func GetRoomTypesHandler(c *gin.Context) {
	db := common.GetDBInstance()
	stmt, err := db.Connection.Prepare("SELECT * FROM room_types WHERE deleted_at is null")
	if err != nil {
		panic(err)
	}
	defer stmt.Close()

	rows, err := stmt.Query()
	if err != nil {
		panic(err)
	}

	var types []common.RoomType
	for rows.Next() {
		var t common.RoomType
		err := rows.Scan(&t.ID, &t.Name, &t.CreatedAt, &t.UpdatedAt, &t.DeletedAt)
		if err = rows.Err(); err != nil {
			panic(err)
		}

		types = append(types, t)
	}

	c.Writer.Header().Set("Content-Type", "application/json")
	c.Writer.Header().Set("Access-Control-Allow-Origin", "*")
	c.Writer.Header().Set("Access-Control-Allow-Headers",
		"Content-Type, Content-Length, Accept-Encoding, X-CSRF-Token, Authorization")
	c.JSON(http.StatusOK, gin.H{
		"message": "OK",
		"data":    types,
	})
}
