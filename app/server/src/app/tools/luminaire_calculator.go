//
// LuminaireCalculator
//
// Summary:
//
// 30 coefficients / 3 categories (10 each - "a", "b", "c")
// Required parameters:
//
//	Input by end-user
//		- Ceiling Color (Input as Hex from color picker - converted to RGB and scaled to 0 to 1 range)
//		- Wall Color (Input as Hex from color picker - converted to RGB and scaled to 0 to 1 range)
// 		- Floor Color (Input as Hex from color picker - converted to RGB and scaled to 0 to 1 range)
// 		- Room Length (in meters)
//		- Room Width (in meters)
//		- Ceiling Height (in meters)
//		- Required luminance (lux)
//
//	Calculated from input colors picked (as scaled RGB)
// 		- Ceiling reflectivity
//		- Wall reflectivity
//		- Floor reflectivity

package tools

import (
	"errors"
	"math"

	"bitbucket.org/presis/brainlit.salesapp/app/server/src/app/common"
)

// LuminaireCalculator object definition
type LuminaireCalculator struct {
	Parameters          *common.CalculationParameters
	CeilingReflectivity float64
	WallReflectivity    float64
	FloorReflectivity   float64
	Formulas            []common.Formula
}

// New Creates a new LuminaireCalculator
func (lc *LuminaireCalculator) New(params *common.CalculationParameters) (*LuminaireCalculator, error) {
	_, err := validateParameters(params)
	if err != nil {
		return nil, err
	}

	// Construct usable RGB values
	params.ConvertCeilingRGB(params.CeilingColor)
	params.ConvertWallRGB(params.WallColor)
	params.ConvertFloorRGB(params.FloorColor)

	return nil, nil
}

// CalculateReflectivity Runs formula to calculate reflectivity of ceiling,
// wall and floor colors
//
// C1 through C20 are the coefficients, R, G, B are sRGB values.
//
// C1*B + C2B^2 + C3*G + C4*G*B + C5*G*B^2 + C6*G^2 + C7*G^2*B + C8*R + C9*R*B+
// C10*R*B^2 + C11*R*G + C12*R*G*B + C13*R*G^2 + C14*R^2 + C15*R^2*B + C16*R^2*G +
// C17*1 + C18*R^3 + C19*G^3 + C20*B^3
//
// Ordinarilly we'd used math.Pow() to x^y, but in order to make the formula's
// editable we've had to use a lib "govaluate" - which doesn't understand math.Pow
// or x^y (even though it states it does accept ^). So for now, we use x*X... for powers
//
func (lc *LuminaireCalculator) CalculateReflectivity(params *common.CalculationParameters,
	formula common.Formula, R float64, G float64, B float64) float64 {
	coeffs := make(map[string]interface{})
	for _, c := range params.CoefficientsR {
		coeffs[c.Name] = c.Value
	}
	coeffs["R"] = R
	coeffs["G"] = G
	coeffs["B"] = B

	C1 := coeffs["C1"].(float64)
	C2 := coeffs["C2"].(float64)
	C3 := coeffs["C3"].(float64)
	C4 := coeffs["C4"].(float64)
	C5 := coeffs["C5"].(float64)
	C6 := coeffs["C6"].(float64)
	C7 := coeffs["C7"].(float64)
	C8 := coeffs["C8"].(float64)
	C9 := coeffs["C9"].(float64)
	C10 := coeffs["C10"].(float64)
	C11 := coeffs["C11"].(float64)
	C12 := coeffs["C12"].(float64)
	C13 := coeffs["C13"].(float64)
	C14 := coeffs["C14"].(float64)
	C15 := coeffs["C15"].(float64)
	C16 := coeffs["C16"].(float64)
	C17 := coeffs["C17"].(float64)
	C18 := coeffs["C18"].(float64)
	C19 := coeffs["C19"].(float64)
	C20 := coeffs["C20"].(float64)

	return C1*B + C2*math.Pow(B, 2) + C3*G + C4*G*B + C5*G*math.Pow(B, 2) + C6*math.Pow(G, 2) + C7*math.Pow(G, 2)*B + C8*R + C9*R*B +
		C10*R*math.Pow(B, 2) + C11*R*G + C12*R*G*B + C13*R*math.Pow(G, 2) + C14*math.Pow(R, 2) + C15*math.Pow(R, 2)*B + C16*math.Pow(R, 2)*G +
		C17*1 + C18*math.Pow(R, 3) + C19*math.Pow(G, 3) + C20*math.Pow(B, 3)

	// return params.CoefficientsR.C1*B + params.CoefficientsR.C2*math.Pow(B, 2) + params.CoefficientsR.C3*G + params.CoefficientsR.C4*G*B +
	// 	params.CoefficientsR.C5*G*math.Pow(B, 2) + params.CoefficientsR.C6*G*math.Pow(G, 2) + params.CoefficientsR.C7*math.Pow(G, 2)*B +
	// 	params.CoefficientsR.C8*R + params.CoefficientsR.C9*R*B + params.CoefficientsR.C10*R*math.Pow(B, 2) + params.CoefficientsR.C11*R*G +
	// 	params.CoefficientsR.C12*R*G*B + params.CoefficientsR.C13*R*math.Pow(G, 2) + params.CoefficientsR.C14*math.Pow(R, 2) +
	// 	params.CoefficientsR.C15*math.Pow(R, 2)*B + params.CoefficientsR.C16*math.Pow(R, 2)*G + params.CoefficientsR.C17*1 +
	// 	params.CoefficientsR.C18*math.Pow(R, 3) + params.CoefficientsR.C19*math.Pow(G, 3) + params.CoefficientsR.C20*math.Pow(B, 3)
	// return common.Evaluate(formula.Formula, coeffs)
}

// CalculateA Calculates the 'A' factor of the luminaire calculation
func (lc *LuminaireCalculator) CalculateA(params *common.CalculationParameters,
	formula common.Formula, C float64, W float64, F float64) float64 {
	coeffs := make(map[string]interface{}, 13)
	for _, c := range params.CoefficientsA {
		coeffs[c.Name] = c.Value
	}
	coeffs["C"] = C
	coeffs["W"] = W
	coeffs["F"] = F

	CA1 := coeffs["CA1"].(float64)
	CA2 := coeffs["CA2"].(float64)
	CA3 := coeffs["CA3"].(float64)
	CA4 := coeffs["CA4"].(float64)
	CA5 := coeffs["CA5"].(float64)
	CA6 := coeffs["CA6"].(float64)
	CA7 := coeffs["CA7"].(float64)
	CA8 := coeffs["CA8"].(float64)
	CA9 := coeffs["CA9"].(float64)
	CA10 := coeffs["CA10"].(float64)

	return CA1*C + CA2*F + CA3*F*C + CA4*W + CA5*W*C + CA6*W*F + CA7 + CA8*math.Pow(W, 2) +
		CA9*math.Pow(F, 2) + CA10*math.Pow(C, 2)
	// return params.CoefficientsA.CA1*C + params.CoefficientsA.CA2*F + params.CoefficientsA.CA3*F*C +
	// 	params.CoefficientsA.CA4*W + params.CoefficientsA.CA5*W*C + params.CoefficientsA.CA6*W*F +
	// 	params.CoefficientsA.CA7 + params.CoefficientsA.CA8*math.Pow(W, 2) + params.CoefficientsA.CA9*math.Pow(F, 2) +
	// 	params.CoefficientsA.CA10*math.Pow(C, 2)
	// return common.Evaluate(formula.Formula, coeffs)
}

// CalculateB Calculates the 'B' factor of the luminaire calculation
func (lc *LuminaireCalculator) CalculateB(params *common.CalculationParameters,
	formula common.Formula, C float64, W float64, F float64) float64 {
	coeffs := make(map[string]interface{}, 13)
	for _, c := range params.CoefficientsB {
		coeffs[c.Name] = c.Value
	}
	coeffs["C"] = C
	coeffs["W"] = W
	coeffs["F"] = F

	CB1 := coeffs["CB1"].(float64)
	CB2 := coeffs["CB2"].(float64)
	CB3 := coeffs["CB3"].(float64)
	CB4 := coeffs["CB4"].(float64)
	CB5 := coeffs["CB5"].(float64)
	CB6 := coeffs["CB6"].(float64)
	CB7 := coeffs["CB7"].(float64)
	CB8 := coeffs["CB8"].(float64)
	CB9 := coeffs["CB9"].(float64)
	CB10 := coeffs["CB10"].(float64)

	return CB1*C + CB2*F + CB3*F*C + CB4*W +
		CB5*W*C + CB6*W*F + CB7 + CB8*math.Pow(W, 2) +
		CB9*math.Pow(F, 2) + CB10*math.Pow(C, 2)
	// return params.CoefficientsB.CB1*C + params.CoefficientsB.CB2*F + params.CoefficientsB.CB3*F*C + params.CoefficientsB.CB4*W +
	// 	params.CoefficientsB.CB5*W*C + params.CoefficientsB.CB6*W*F + params.CoefficientsB.CB7 + params.CoefficientsB.CB8*math.Pow(W, 2) +
	// 	params.CoefficientsB.CB9*math.Pow(F, 2) + params.CoefficientsB.CB10*math.Pow(C, 2)
	// return common.Evaluate(formula.Formula, coeffs)
}

// CalculateC Calculates the 'C' factor of the luminaire calculation
func (lc *LuminaireCalculator) CalculateC(params *common.CalculationParameters,
	formula common.Formula, C float64, W float64, F float64) float64 {
	coeffs := make(map[string]interface{}, 13)
	for _, c := range params.CoefficientsC {
		coeffs[c.Name] = c.Value
	}
	coeffs["C"] = C
	coeffs["W"] = W
	coeffs["F"] = F

	CC1 := coeffs["CC1"].(float64)
	CC2 := coeffs["CC2"].(float64)
	CC3 := coeffs["CC3"].(float64)
	CC4 := coeffs["CC4"].(float64)
	CC5 := coeffs["CC5"].(float64)
	CC6 := coeffs["CC6"].(float64)
	CC7 := coeffs["CC7"].(float64)
	CC8 := coeffs["CC8"].(float64)
	CC9 := coeffs["CC9"].(float64)
	CC10 := coeffs["CC10"].(float64)

	return CC1*C + CC2*F + CC3*F*C + CC4*W +
		CC5*W*C + CC6*W*F + CC7 + CC8*math.Pow(W, 2) +
		CC9*math.Pow(F, 2) + CC10*math.Pow(C, 2)

	// return params.CoefficientsC.CC1*C + params.CoefficientsC.CC2*F + params.CoefficientsC.CC3*F*C + params.CoefficientsC.CC4*W +
	// 	params.CoefficientsC.CC5*W*C + params.CoefficientsC.CC6*W*F + params.CoefficientsC.CC7 + params.CoefficientsC.CC8*math.Pow(W, 2) +
	// 	params.CoefficientsC.CC9*math.Pow(F, 2) + params.CoefficientsC.CC10*math.Pow(C, 2)
	// return common.Evaluate(formula.Formula, coeffs)
}

// CalculateRoomIndex Calculates the index value for the combined room dimensions
func (lc *LuminaireCalculator) CalculateRoomIndex(width float32, length float32, ceilingHeight float32) float32 {
	var Hm = (ceilingHeight - 0.85)
	return width * length / ((width + length) * Hm)
}

// CalculateUF Calculates the utilization factor for the previously calculated roomIndex
func (lc *LuminaireCalculator) CalculateUF(k float32, a float64, b float64, c float64) float64 {
	return a*math.Pow(float64(k), b) + c
}

// CalculateLuminaires Runs formula to calculate the number of luminaires required
func (lc *LuminaireCalculator) CalculateLuminaires(requiredLux float64, area float64, lumenOutput float64, uf float64) float64 {
	return math.Round(requiredLux * area / (lumenOutput * uf))
}

// CalculatePricing Runs formula to calculate total cost of previously calculated luminaire total
func (lc *LuminaireCalculator) CalculatePricing() {
	// TODO - Add this function in when model is complete
}

// validateParameters Ensures that passed parameters are complete and correct
func validateParameters(params *common.CalculationParameters) (bool, error) {
	if len(params.CeilingColor) == 0 {
		return false, errors.New("No CeilingColor specified")
	}
	if len(params.WallColor) == 0 {
		return false, errors.New("No WallColor specified")
	}
	if len(params.FloorColor) == 0 {
		return false, errors.New("No FloorColor specified")
	}
	if params.RoomLength < 0 {
		return false, errors.New("RoomLength is invalid")
	}
	if params.RoomWidth < 0 {
		return false, errors.New("RoomWidth is invalid")
	}
	if params.CeilingHeight < 0 {
		return false, errors.New("CeilingHeight is invalid")
	}
	if params.RequiredLuminance < 0 {
		return false, errors.New("RequiredLuminance is invalid")
	}
	return true, nil
}
